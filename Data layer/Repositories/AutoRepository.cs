﻿// <copyright file="AutoRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Data_Layer.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Enums;
    using Interfaces;

    /// <summary>
    /// Autó repository
    /// </summary>
    public class AutoRepository : ICarRepository<Auto>
    {
        /// <summary>
        /// Adatbázis
        /// </summary>
        private AdatbazisEntities entities;

        /// <summary>
        /// Initializes a new instance of the <see cref="AutoRepository"/> class.
        /// Konstruktor
        /// </summary>
        /// <param name="entities">adatbázis</param>
        public AutoRepository(AdatbazisEntities entities)
        {
            this.entities = entities;
        }

        /// <summary>
        /// Új autó felvétele
        /// </summary>
        /// <param name="newCar">új autó</param>
        /// <param name="brand">új autó márkája</param>
        public void Add(Auto newCar, string brand)
        {
            try
            {
                this.ThrowIfExists(newCar.rendszam);

                // ha az éppen felvett autó típusa még nincs a rendszerben, akkor felvesszük a típust és a márkát az adatbázisba
                if (this.entities.AutoTipus.Where(x => x.tipus == newCar.tipus).Count() == 0)
                {
                    this.entities.AutoTipus.Add(new AutoTipus() { tipus = newCar.tipus, marka = brand });
                }

                this.entities.Auto.Add(newCar);
                this.entities.SaveChanges();
            }
            catch (InvalidOperationException)
            {
                throw;
            }
        }

        /// <summary>
        /// Paraméterül kapott azonosítóval rendelkező elem törlése.
        /// </summary>
        /// <param name="toDelete">törlendő elem azoonosítója</param>
        public void Delete(string toDelete)
        {
            // csak akkor töröljük ha nem szerepel se az eladott autók közt sem a kölcsönzöttek közt
            if (this.entities.EladottAuto.Where(x => x.rendszam == toDelete).Count() == 0 && this.entities.KolcsonzottAuto.Where(x => x.rendszam == toDelete).Count() == 0)
            {
                Auto auto = this.GetById(toDelete);
                this.entities.Auto.Remove(auto);
                this.entities.SaveChanges();
            }
            else
            {
                throw new InvalidOperationException("Autó nem törölhető, további függőségi viszonyok állnak fent.");
            }
        }

        /// <summary>
        /// Összes elem visszadaása
        /// </summary>
        /// <returns>Autók</returns>
        public IQueryable<Auto> GetAll()
        {
            return this.entities.Auto;
        }

        /// <summary>
        /// Paraméterül kapott azonosítóval rendelkező elem visszadása
        /// </summary>
        /// <param name="id">azonosító</param>
        /// <returns>Autó</returns>
        public Auto GetById(string id)
        {
            return this.entities.Auto.Single(x => x.rendszam == id);
        }

        /// <summary>
        /// Frissíti az adatbázis az aktuális dátumnak megfelelően (kölcsönözve --> elérhető)
        /// </summary>
        public void UpdateSatus() // meg kell hívni a program indításakor hogy mindig friss legyen az adatbázis, abból a szempontból hogy melyik autó elérhető és melyik nem
        {
            foreach (Auto auto in this.GetAll())
            {
                if (auto.statusz == CarStatusEnum.kölcsönözve.ToString())
                {
                    string rendszam = auto.rendszam;

                    if (DateTime.Now.Subtract(this.entities.KolcsonzottAuto.Find(rendszam).kezdeti_datum + new TimeSpan(decimal.ToInt32(this.entities.KolcsonzottAuto.Find(rendszam).kolcsonzott_napok), 23, 59, 59)) > new TimeSpan(0, 0, 0, 0))
                    {
                        auto.statusz = CarStatusEnum.elérheto.ToString();
                    }
                }
            }
        }

        /// <summary>
        /// Visszaadja azon autókat amelyeknek a státusza a paraméterrel megyegyezik
        /// </summary>
        /// <param name="status">státusz</param>
        /// <returns>Autók</returns>
        public IEnumerable<Auto> GetCarsByStatus(CarStatusEnum status)
        {
            return this.entities.Auto.Where(x => x.statusz == status.ToString());
        }

        private void ThrowIfExists(string id)
        {
            if (this.entities.Auto.Any(x => x.rendszam == id))
            {
                throw new InvalidOperationException("Ilyen autó már létezik!");
            }
        }
    }
}
