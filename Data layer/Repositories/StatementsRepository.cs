﻿// <copyright file="StatementsRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Data_Layer
{
    using System;
    using System.Linq;
    using Data_Layer.Interfaces;

    /// <summary>
    /// Kimutatások
    /// </summary>
    public class StatementsRepository : IStatements
    {
        private AdatbazisEntities entities;

        /// <summary>
        /// Initializes a new instance of the <see cref="StatementsRepository"/> class.
        /// Konstruktor
        /// </summary>
        /// <param name="entities">adatbázis</param>
        public StatementsRepository(AdatbazisEntities entities)
        {
            this.entities = entities;
        }

        /// <summary>
        /// Kölcsönzött autók száma adott intervallumba
        /// </summary>
        /// <param name="from">intervallum eleje</param>
        /// <param name="till">intervallum vége</param>
        /// <returns>darab</returns>
        public int RentedCarsCount(DateTime from, DateTime till)
        {
            return this.RentedCars(from, till).Count();
        }

        /// <summary>
        /// Eladott autók száma, adott intervallumba
        /// </summary>
        /// <param name="from">intervallum eleje</param>
        /// <param name="till">intervallum vége</param>
        /// <returns>darab</returns>
        public int SoldCarsCount(DateTime from, DateTime till)
        {
            return this.SoldCars(from, till).Count();
        }

        /// <summary>
        /// Eladott autókból  származó bevétel adott intervallumba
        /// </summary>
        /// <param name="from">intervallum eleje</param>
        /// <param name="till">intervallum vége</param>
        /// <returns>bevétel</returns>
        public int SoldCarsIncome(DateTime from, DateTime till)
        {
            try
            {
                return decimal.ToInt32(this.SoldCars(from, till).Sum(x => x.eladasi_ar));
            }
            catch (InvalidOperationException)
            {
                return 0;
            }
        }

        /// <summary>
        /// Kölcsönzött auatókból származó bevétel adott intervallumba
        /// </summary>
        /// <param name="from">intervallum eleje</param>
        /// <param name="till">intervallum vége</param>
        /// <returns>bevétel</returns>
        public int RentedCarsIncome(DateTime from, DateTime till)
        {
            try
            {
                return decimal.ToInt32(this.RentedCars(from, till).Sum(x => x.kalkulalt_ar));
            }
            catch (InvalidOperationException)
            {
                return 0;
            }
        }

        private IQueryable<EladottAuto> SoldCars(DateTime from, DateTime till)
        {
            return this.entities.EladottAuto.Where(x => x.eladasi_datum >= from && x.eladasi_datum <= till);
        }

        private IQueryable<KolcsonzottAuto> RentedCars(DateTime from, DateTime till)
        {
            return this.entities.KolcsonzottAuto.Where(x => x.kezdeti_datum >= from && x.kezdeti_datum <= till);
        }
    }
}
