﻿// <copyright file="FelhasznaloRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Data_Layer.Repositories
{
    using System;
    using System.Linq;
    using Data_Layer.Interfaces;

    /// <summary>
    /// felhasználó repository
    /// </summary>
    public class FelhasznaloRepository : IUserRepository<Felhasznalo>
    {
        /// <summary>
        /// Adatbázis
        /// </summary>
        private AdatbazisEntities entities;

        /// <summary>
        /// Initializes a new instance of the <see cref="FelhasznaloRepository"/> class.
        /// Konstruktor
        /// </summary>
        /// <param name="entities">adatbázis</param>
        public FelhasznaloRepository(AdatbazisEntities entities)
        {
            this.entities = entities;
        }

        /// <summary>
        /// Paraméterül kapott elem felvétele
        /// </summary>
        /// <param name="toSave">felvételre szánt objektum</param>
        public void Add(Felhasznalo toSave)
        {
            try
            {
                this.ThrowIfExists(toSave.felhasznalonev);
                toSave.jelszo = Utilities.GetHashSha256(toSave.jelszo);
                this.entities.Felhasznalo.Add(toSave);
                this.entities.SaveChanges();
            }
            catch (InvalidOperationException)
            {
                throw;
            }
        }

        /// <summary>
        /// Paraméterül kapott azonosítóval rendelkező elem törlése.
        /// </summary>
        /// <param name="toDelete">törlendő elem azoonosítója</param>
        public void Delete(string toDelete)
        {
            this.entities.Felhasznalo.Remove(this.GetById(toDelete));
            this.entities.SaveChanges();
        }

        /// <summary>
        /// Összes elem visszadaása
        /// </summary>
        /// <returns>Felhasználók</returns>
        public IQueryable<Felhasznalo> GetAll()
        {
            return this.entities.Felhasznalo;
        }

        /// <summary>
        /// Paraméterül kapott azonosítóval rendelkező elem visszadása
        /// </summary>
        /// <param name="id">azonosító</param>
        /// <returns>Felhasználó</returns>
        public Felhasznalo GetById(string id)
        {
            try
            {
                return this.entities.Felhasznalo.Single(x => x.felhasznalonev == id);
            }
            catch (InvalidOperationException)
            {
                throw new LoginException(id, "Nincs ilyen felhasználónév");
            }
        }

        /// <summary>
        /// Visszaadja, hogy sikeres volt e az azonosítás
        /// </summary>
        /// <param name="username">bejelentkező felhasználóneve</param>
        /// <param name="pass">bejelentkező jelszava</param>
        /// <returns>bejelentkezés sikeressége</returns>
        public bool VerifyUser(string username, string pass)
        {
            string sha256 = Utilities.GetHashSha256(pass);
            return this.entities.Felhasznalo.Where(x => x.felhasznalonev == username && x.jelszo == sha256).Count() == 1;
        }

        private void ThrowIfExists(string id)
        {
            if (this.entities.Felhasznalo.Any(x => x.felhasznalonev == id))
            {
                throw new InvalidOperationException("Ilyen felhasználónév már létezik!");
            }
        }
    }
}
