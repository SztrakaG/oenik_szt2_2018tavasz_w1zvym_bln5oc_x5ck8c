﻿// <copyright file="TransactionRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Data_Layer.Repositories
{
    using System.Collections.Generic;
    using System.Linq;
    using Interfaces;

    /// <summary>
    /// Tranzakciókat tartalmazó repository
    /// </summary>
    public class TransactionRepository : ITransactionRepository
    {
        private List<Tranzakcio> tranzakciok;
        private AdatbazisEntities entities;

        /// <summary>
        /// Initializes a new instance of the <see cref="TransactionRepository"/> class.
        /// Tranzakciók repositoryt hozza létre
        /// </summary>
        /// <param name="entities">adatbázis</param>
        public TransactionRepository(AdatbazisEntities entities)
        {
            this.tranzakciok = new List<Tranzakcio>();
            this.entities = entities;
            this.GenerateTransactions();
        }

        /// <summary>
        /// Gets összes tranzakció
        /// </summary>
        public List<Tranzakcio> Tranzakciok
        {
            get
            {
                return this.tranzakciok;
            }
        }

        private void GenerateTransactions()
        {
            var sold = from eladott in this.entities.EladottAuto
                         join ugyfel in this.entities.Ugyfel
                         on eladott.adoszam equals ugyfel.adoszam
                         join marka in this.entities.AutoTipus on eladott.Auto.tipus equals marka.tipus
                         select new { Nev = ugyfel.ugyfel_nev, Adoszam = eladott.adoszam, Rendszam = eladott.rendszam, Marka = marka.marka, Tipus = eladott.Auto.tipus, Ar = (int)eladott.eladasi_ar, Muvelet = "eladva", Napok = string.Empty, eladott.eladasi_datum };
            foreach (var item in sold)
            {
                this.tranzakciok.Add(new Tranzakcio(item.Nev, item.Adoszam, item.Rendszam, item.Marka, item.Tipus, item.Ar, item.Muvelet, item.Napok, item.eladasi_datum));
            }

            var rent = from kolcsonzott in this.entities.KolcsonzottAuto
                         join ugyfel in this.entities.Ugyfel
                         on kolcsonzott.adoszam equals ugyfel.adoszam
                         join marka in this.entities.AutoTipus on kolcsonzott.Auto.tipus equals marka.tipus
                         select new { Nev = ugyfel.ugyfel_nev, Adoszam = kolcsonzott.adoszam, Rendszam = kolcsonzott.rendszam, Marka = marka.marka, Tipus = kolcsonzott.Auto.tipus, Ar = kolcsonzott.Auto.kolcsonzesi_ar_1napra * kolcsonzott.kolcsonzott_napok, Muvelet = "kölcsönözve", Napok = kolcsonzott.kolcsonzott_napok, kolcsonzott.kezdeti_datum };
            foreach (var item in rent)
            {
                this.tranzakciok.Add(new Tranzakcio(item.Nev, item.Adoszam, item.Rendszam, item.Marka, item.Tipus, decimal.ToInt32(item.Ar), item.Muvelet, item.Napok.ToString(), item.kezdeti_datum));
            }
        }
    }
}
