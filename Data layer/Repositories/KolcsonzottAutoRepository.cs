﻿// <copyright file="KolcsonzottAutoRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Data_Layer.Repositories
{
    using System;
    using System.Linq;
    using Enums;
    using Interfaces;

    /// <summary>
    /// kölcsönzött autó repository
    /// </summary>
    public class KolcsonzottAutoRepository : IRentCarRepository<Auto, Ugyfel, KolcsonzottAuto>
    {
        /// <summary>
        /// Adatbázis
        /// </summary>
        private AdatbazisEntities entities;

        /// <summary>
        /// Initializes a new instance of the <see cref="KolcsonzottAutoRepository"/> class.
        /// Konstruktor
        /// </summary>
        /// <param name="entities">adatbázis</param>
        public KolcsonzottAutoRepository(AdatbazisEntities entities)
        {
            this.entities = entities;
        }

        /// <summary>
        /// Kölcsönzött autó felvétele a rendszerbe
        /// </summary>
        /// <param name="toRent">kölcsönzendő autó</param>
        /// <param name="renter">kölcsönző</param>
        /// <param name="days">kölcsönzési napok száma</param>
        public void Add(Auto toRent, Ugyfel renter, int days)
        {
            KolcsonzottAuto kolcsonAuto = new KolcsonzottAuto();
            kolcsonAuto.rendszam = toRent.rendszam;
            kolcsonAuto.adoszam = renter.adoszam;
            kolcsonAuto.kezdeti_datum = DateTime.Now;
            kolcsonAuto.kolcsonzott_napok = days;
            kolcsonAuto.kalkulalt_ar = toRent.kolcsonzesi_ar_1napra * days * ((renter.torzsvasarloi_pontszam / 2) / 100);
            if (renter.torzsvasarloi_pontszam + 1 <= 20)
            {
                renter.torzsvasarloi_pontszam++;
            }

            this.entities.Auto.Find(toRent.rendszam).statusz = CarStatusEnum.kölcsönözve.ToString();
            this.entities.KolcsonzottAuto.Add(kolcsonAuto);
            this.entities.SaveChanges();
        }

        /// <summary>
        /// Paraméterül kapott azonosítóval rendelkező elem törlése.
        /// </summary>
        /// <param name="toDelete">törlendő elem azoonosítója</param>
        public void Delete(string toDelete)
        {
            this.entities.KolcsonzottAuto.Remove(this.GetById(toDelete));
            this.entities.SaveChanges();
        }

        /// <summary>
        /// Összes elem visszadaása
        /// </summary>
        /// <returns>Kölcsönzött autók</returns>
        public IQueryable<KolcsonzottAuto> GetAll()
        {
            return this.entities.KolcsonzottAuto;
        }

        /// <summary>
        /// Paraméterül kapott azonosítóval rendelkező elem visszadása
        /// </summary>
        /// <param name="id">azonosító</param>
        /// <returns>Kölcsönzött autó</returns>
        public KolcsonzottAuto GetById(string id)
        {
            try
            {
                return this.entities.KolcsonzottAuto.Single(x => x.kolcsonzesID == decimal.Parse(id));
            }
            catch (InvalidOperationException)
            {
                throw new LoginException(id, "Nincs ilyen rendszámú autó a rendszerben!");
            }
        }
    }
}
