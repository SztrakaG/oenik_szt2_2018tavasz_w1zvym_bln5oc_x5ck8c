﻿// <copyright file="EladottAutoRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Data_Layer.Repositories
{
    using System;
    using System.Linq;
    using Data_Layer.Enums;
    using Data_Layer.Interfaces;

    /// <summary>
    /// eladott autó repository
    /// </summary>
    public class EladottAutoRepository : ISellCarRepository<Auto, Ugyfel, EladottAuto>
    {
        /// <summary>
        /// Adatbázis
        /// </summary>
        private AdatbazisEntities entities;

        /// <summary>
        /// Initializes a new instance of the <see cref="EladottAutoRepository"/> class.
        /// Konstruktor
        /// </summary>
        /// <param name="entities">adatbázis</param>
        public EladottAutoRepository(AdatbazisEntities entities)
        {
            this.entities = entities;
        }

        /// <summary>
        /// Autó felvétele az eladott autók közé
        /// </summary>
        /// <param name="toSell">eladott autó</param>
        /// <param name="buyer">vevő</param>
        public void Add(Auto toSell, Ugyfel buyer)
        {
            EladottAuto eladott = new EladottAuto();
            eladott.rendszam = toSell.rendszam;
            eladott.adoszam = buyer.adoszam;
            eladott.eladasi_datum = DateTime.Now;
            eladott.eladasi_ar = (decimal)toSell.alapar * (1 - ((buyer.torzsvasarloi_pontszam / 2) / 100)); // max 20 pontja lehet, a pontjainak a fele fog %-os kedvezménynek minősülni, tehát ez max 10% lehet
            eladott.kedvezmeny = (int)(toSell.alapar * ((buyer.torzsvasarloi_pontszam / 2) / 100)) / toSell.alapar * 100;
            if (buyer.torzsvasarloi_pontszam + 2 <= 20)
            {
                buyer.torzsvasarloi_pontszam += 2;
            }

            this.entities.EladottAuto.Add(eladott);
            this.entities.Auto.Find(toSell.rendszam).statusz = CarStatusEnum.eladva.ToString();
            this.entities.SaveChanges();
        }

        /// <summary>
        /// Paraméterül kapott azonosítóval rendelkező elem törlése.
        /// </summary>
        /// <param name="toDelete">törlendő elem azoonosítója</param>
        public void Delete(string toDelete)
        {
            this.entities.EladottAuto.Remove(this.GetById(toDelete));
            this.entities.Auto.Find(toDelete).statusz = CarStatusEnum.elérheto.ToString();
            this.entities.SaveChanges();
        }

        /// <summary>
        /// Összes elem visszadaása
        /// </summary>
        /// <returns>Autók</returns>
        public IQueryable<EladottAuto> GetAll()
        {
            return this.entities.EladottAuto;
        }

        /// <summary>
        /// Paraméterül kapott azonosítóval rendelkező elem visszadása
        /// </summary>
        /// <param name="id">azonosító</param>
        /// <returns>Autó</returns>
        public EladottAuto GetById(string id)
        {
            return this.entities.EladottAuto.Single(x => x.rendszam == id);
        }
    }
}
