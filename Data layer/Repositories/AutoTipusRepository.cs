﻿// <copyright file="AutoTipusRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Data_Layer.Repositories
{
    using System.Linq;
    using Interfaces;

    /// <summary>
    /// Autó típus repository
    /// </summary>
    public class AutoTipusRepository : ISimpleRepository<AutoTipus>
    {
        /// <summary>
        /// Adatbázis
        /// </summary>
        private AdatbazisEntities entities;

        /// <summary>
        /// Initializes a new instance of the <see cref="AutoTipusRepository"/> class.
        /// Konstruktor
        /// </summary>
        /// <param name="entities">adatbázis</param>
        public AutoTipusRepository(AdatbazisEntities entities)
        {
            this.entities = entities;
        }

        /// <summary>
        /// Paraméterül kapott elem felvétele
        /// </summary>
        /// <param name="toSave">felvételre szánt objektum</param>
        public void Add(AutoTipus toSave)
        {
            this.entities.AutoTipus.Add(toSave);
            this.entities.SaveChanges();
        }

        /// <summary>
        /// Paraméterül kapott azonosítóval rendelkező elem törlése.
        /// </summary>
        /// <param name="toDelete">törlendő elem azoonosítója</param>
        public void Delete(string toDelete)
        {
            this.entities.AutoTipus.Remove(this.GetById(toDelete));
            this.entities.SaveChanges();
        }

        /// <summary>
        /// Összes elem visszadaása
        /// </summary>
        /// <returns>Autók</returns>
        public IQueryable<AutoTipus> GetAll()
        {
            return this.entities.AutoTipus;
        }

        /// <summary>
        /// Paraméterül kapott azonosítóval rendelkező elem visszadása
        /// </summary>
        /// <param name="id">azonosító</param>
        /// <returns>Autó</returns>
        public AutoTipus GetById(string id)
        {
            return this.entities.AutoTipus.Single(x => x.tipus == id);
        }
    }
}
