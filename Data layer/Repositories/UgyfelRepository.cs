﻿// <copyright file="UgyfelRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Data_Layer.Repositories
{
    using System;
    using System.Linq;
    using Data_Layer.Interfaces;

    /// <summary>
    /// ügyfél repository
    /// </summary>
    public class UgyfelRepository : IClientRepository<Ugyfel, Kapcsolattarto>
    {
        /// <summary>
        /// Adatbázis
        /// </summary>
        private AdatbazisEntities entities;

        /// <summary>
        /// Initializes a new instance of the <see cref="UgyfelRepository"/> class.
        /// Konstruktor
        /// </summary>
        /// <param name="entities">adatbázis</param>
        public UgyfelRepository(AdatbazisEntities entities)
        {
            this.entities = entities;
        }

        /// <summary>
        /// Paraméterül kapott kliens felvétele
        /// </summary>
        /// <param name="ugyfel">ügyfél</param>
        /// <param name="kapcsolattarto">ügyfél kapcsolattartója</param>
        public void Add(Ugyfel ugyfel, Kapcsolattarto kapcsolattarto)
        {
            try
            {
                this.ThrowIfExists(ugyfel.adoszam);

                // még nincs ilyen kapcsolattartó a rendszerben
                if (this.entities.Kapcsolattarto.Where(x => x.szigszam == kapcsolattarto.szigszam).Count() == 0)
                {
                    this.entities.Kapcsolattarto.Add(kapcsolattarto);
                }

                this.entities.Ugyfel.Add(ugyfel);
                this.entities.SaveChanges();
            }
            catch (InvalidOperationException)
            {
                throw;
            }
        }

        /// <summary>
        /// Paraméterül kapott azonosítóval rendelkező elem törlése.
        /// </summary>
        /// <param name="toDelete">törlendő elem azoonosítója</param>
        public void Delete(string toDelete)
        {
            if (this.entities.EladottAuto.Where(x => x.adoszam == toDelete).Count() > 0 || this.entities.KolcsonzottAuto.Where(x => x.adoszam == toDelete).Count() > 0)
            {
                throw new InvalidOperationException("Az ügyfél nem törölhető!");
            }

            this.entities.Ugyfel.Remove(this.GetById(toDelete));
            this.entities.SaveChanges();
        }

        /// <summary>
        /// Összes elem visszadaása
        /// </summary>
        /// <returns>Ügyfelek</returns>
        public IQueryable<Ugyfel> GetAll()
        {
            return this.entities.Ugyfel;
        }

        /// <summary>
        /// Paraméterül kapott azonosítóval rendelkező elem visszadása
        /// </summary>
        /// <param name="id">azonosító</param>
        /// <returns>Ügyfél</returns>
        public Ugyfel GetById(string id)
        {
            return this.entities.Ugyfel.Single(x => x.adoszam == id);
        }

        private void ThrowIfExists(string id)
        {
            if (this.entities.Ugyfel.Any(x => x.adoszam == id))
            {
                throw new InvalidOperationException("Ilyen ügyfél már létezik!");
            }
        }
    }
}
