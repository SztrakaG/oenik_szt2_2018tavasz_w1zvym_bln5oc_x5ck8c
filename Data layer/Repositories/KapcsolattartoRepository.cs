﻿// <copyright file="KapcsolattartoRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Data_Layer.Repositories
{
    using System;
    using System.Linq;
    using Interfaces;

    /// <summary>
    /// kapcsolattartó repository
    /// </summary>
    public class KapcsolattartoRepository : IRepository<Kapcsolattarto>
    {
        private AdatbazisEntities entities;

        /// <summary>
        /// Initializes a new instance of the <see cref="KapcsolattartoRepository"/> class.
        /// Konstruktor
        /// </summary>
        /// <param name="entities">adatbázis</param>
        public KapcsolattartoRepository(AdatbazisEntities entities)
        {
            this.entities = entities;
        }

        /// <summary>
        /// Paraméterül kapott elem felvétele
        /// </summary>
        /// <param name="toSave">felvételre szánt objektum</param>
        public void Add(Kapcsolattarto toSave)
        {
            this.entities.Kapcsolattarto.Add(toSave);
            this.entities.SaveChanges();
        }

        /// <summary>
        /// Paraméterül kapott azonosítóval rendelkező elem törlése.
        /// </summary>
        /// <param name="toDelete">törlendő elem azoonosítója</param>
        public void Delete(string toDelete)
        {
            if (this.entities.Ugyfel.Where(x => x.szigszam == toDelete).Count() > 0)
            {
                throw new InvalidOperationException("A kapcsolattartó nem törölhető!");
            }

            this.entities.Kapcsolattarto.Remove(this.GetById(toDelete));
            this.entities.SaveChanges();
        }

        /// <summary>
        /// Összes elem visszadaása
        /// </summary>
        /// <returns>Kapcsolattartók</returns>
        public IQueryable<Kapcsolattarto> GetAll()
        {
            return this.entities.Kapcsolattarto;
        }

        /// <summary>
        /// Paraméterül kapott azonosítóval rendelkező elem visszadása
        /// </summary>
        /// <param name="id">azonosító</param>
        /// <returns>Kapcsolattartó</returns>
        public Kapcsolattarto GetById(string id)
        {
            return this.entities.Kapcsolattarto.Single(x => x.szigszam == id);
        }
    }
}
