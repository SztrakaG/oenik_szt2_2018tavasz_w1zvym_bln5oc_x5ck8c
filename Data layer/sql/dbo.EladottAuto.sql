﻿CREATE TABLE [dbo].[EladottAuto] (
    [Rendszam]      VARCHAR (10) NOT NULL,
    [Adoszam]       VARCHAR (20) NOT NULL,
    [EladasiDatum] DATETIME     NOT NULL,
    [EladasiAR]    NUMERIC (10) NOT NULL,
    [Kedvezmeny]    NUMERIC (3)  NOT NULL,
    CONSTRAINT [pk_eladottauto] PRIMARY KEY CLUSTERED ([Rendszam] ASC),
    CONSTRAINT [fk_eladottauto] FOREIGN KEY ([Rendszam]) REFERENCES [dbo].[Auto] ([Rendszam]),
    CONSTRAINT [fk2_eladottauto] FOREIGN KEY ([Adoszam]) REFERENCES [dbo].[Ugyfel] ([Adoszam])
);

