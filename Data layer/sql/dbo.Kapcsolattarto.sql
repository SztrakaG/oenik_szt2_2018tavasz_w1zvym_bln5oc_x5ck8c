﻿CREATE TABLE [dbo].[Kapcsolattarto] (
    [Szigszam]           VARCHAR (15) NOT NULL,
    [KapcsolattartoNev] VARCHAR (50) NOT NULL,
    [Telefonszam]        VARCHAR (20) NOT NULL,
    [Email]              VARCHAR (64) NOT NULL,
    CONSTRAINT [pk_kapcsolattarto] PRIMARY KEY CLUSTERED ([Szigszam] ASC)
);

