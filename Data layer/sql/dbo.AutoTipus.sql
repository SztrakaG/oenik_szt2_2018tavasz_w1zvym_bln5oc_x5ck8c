﻿CREATE TABLE [dbo].[AutoTipus] (
    [Tipus] VARCHAR (15) NOT NULL,
    [Marka] VARCHAR (15) NOT NULL,
    CONSTRAINT [pk_autotipus] PRIMARY KEY CLUSTERED ([Tipus] ASC)
);

