﻿CREATE TABLE [dbo].[KolcsonzottAuto] (
    [KolcsonzesId]      NUMERIC (5)  IDENTITY (1, 1) NOT NULL,
    [Rendszam]          VARCHAR (10) NOT NULL,
    [Adoszam]           VARCHAR (20) NOT NULL,
    [KezdetiDatum]     DATETIME     NOT NULL,
    [KolcsonzottNapok] NUMERIC (3)  NOT NULL,
    [KalkulaltAr]      NUMERIC (10) NOT NULL,
    CONSTRAINT [pk_kolcsonzottauto] PRIMARY KEY CLUSTERED ([KolcsonzesId] ASC),
    CONSTRAINT [fk_kolcsonzottauto] FOREIGN KEY ([Rendszam]) REFERENCES [dbo].[Auto] ([Rendszam]),
    CONSTRAINT [fk2_kolcsonzottauto] FOREIGN KEY ([Adoszam]) REFERENCES [dbo].[Ugyfel] ([Adoszam])
);

