﻿CREATE TABLE [dbo].[Auto] (
    [Rendszam]              VARCHAR (10) NOT NULL,
    [Tipus]                 VARCHAR (15) NOT NULL,
    [Alapar]                NUMERIC (10) NOT NULL,
    [Szin]                  VARCHAR (20) NOT NULL,
    [KolcsonzesiArNapra] NUMERIC (10) NOT NULL,
    [Allapot]               VARCHAR (15) NOT NULL,
    [Statusz]               VARCHAR (15) NOT NULL,
    [FelveteliDatum]       DATETIME     NOT NULL,
    CONSTRAINT [pk_auto] PRIMARY KEY CLUSTERED ([Rendszam] ASC),
    CONSTRAINT [fk_auto] FOREIGN KEY ([Tipus]) REFERENCES [dbo].[AutoTipus] ([Tipus])
);

