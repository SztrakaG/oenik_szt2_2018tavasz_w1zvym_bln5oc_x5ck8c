﻿CREATE TABLE [dbo].[Ugyfel] (
    [Adoszam]                VARCHAR (20) NOT NULL,
    [Szigszam]               VARCHAR (15) NOT NULL,
    [UgyfelNev]             VARCHAR (64) NOT NULL,
    [TorzsvasarloiPontszam] NUMERIC (2)  NOT NULL,
    CONSTRAINT [pk_ugyfel] PRIMARY KEY CLUSTERED ([Adoszam] ASC),
    CONSTRAINT [fk_ugyfel] FOREIGN KEY ([Szigszam]) REFERENCES [dbo].[Kapcsolattarto] ([Szigszam])
);

