﻿CREATE TABLE [dbo].[Felhasznalo] (
    [Felhasznalonev] VARCHAR (20) NOT NULL,
    [Jelszo]         VARCHAR (20) NOT NULL,
    [Jogosultsag]    VARCHAR (15) NOT NULL,
    [Aktiv]          BIT          NOT NULL,
    CONSTRAINT [pk_felhasznalo] PRIMARY KEY CLUSTERED ([Felhasznalonev] ASC)
);

