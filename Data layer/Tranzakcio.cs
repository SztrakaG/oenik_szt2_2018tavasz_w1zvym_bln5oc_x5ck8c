﻿// <copyright file="Tranzakcio.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Data_Layer
{
    using System;

    /// <summary>
    /// Tranzakció modell
    /// </summary>
    public class Tranzakcio
    {
        private string nev;
        private string adoszam;
        private string rendszam;
        private string marka;
        private string tipus;
        private int ar;
        private string muvelet;
        private string napok;
        private DateTime datum;

        /// <summary>
        /// Initializes a new instance of the <see cref="Tranzakcio"/> class.
        /// Létrehoz egy tranzakció objektumot
        /// </summary>
        /// <param name="nev">ügyfél neve</param>
        /// <param name="adoszam">ügyfél adószáma</param>
        /// <param name="marka">autó márkája</param>
        /// <param name="tipus">autó típusa</param>
        /// <param name="ar">autó ára</param>
        /// <param name="muvelet">eladás/kölcsönzés</param>
        /// <param name="napok">kölcsönzött napok száma</param>
        /// <param name="datum">kölcsönzés/eladás dátuma</param>
        public Tranzakcio(string nev, string adoszam, string rendszam, string marka, string tipus, int ar, string muvelet, string napok, DateTime datum)
        {
            this.nev = nev;
            this.adoszam = adoszam;
            this.rendszam = rendszam;
            this.marka = marka;
            this.tipus = tipus;
            this.ar = ar;
            this.muvelet = muvelet;
            this.napok = napok;
            this.datum = datum;
        }

        /// <summary>
        /// Gets autóhoz köhető név
        /// </summary>
        public string Nev
        {
            get
            {
                return this.nev;
            }
        }

        /// <summary>
        /// Gets autóhoz köthető adószám
        /// </summary>
        public string Adoszam
        {
            get
            {
                return this.adoszam;
            }
        }

        /// <summary>
        /// Gets autó rendszáma
        /// </summary>
        public string Rendszam
        {
            get
            {
                return this.rendszam;
            }
        }

        /// <summary>
        /// Gets autó márkája
        /// </summary>
        public string Marka
        {
            get
            {
                return this.marka;
            }
        }

        /// <summary>
        /// Gets autó típusa
        /// </summary>
        public string Tipus
        {
            get
            {
                return this.tipus;
            }
        }

        /// <summary>
        /// Gets autó ára
        /// </summary>
        public int AR
        {
            get
            {
                return this.ar;
            }
        }

        /// <summary>
        /// Gets eladás volt e az adott tranzakció vagy kölcsönzés
        /// </summary>
        public string Muvelet
        {
            get
            {
                return this.muvelet;
            }
        }

        /// <summary>
        /// Gets kölcsönzés esetén a napok száma
        /// </summary>
        public string Napok
        {
            get
            {
                return this.napok;
            }
        }

        /// <summary>
        /// Gets tranzakció dátuma
        /// </summary>
        public DateTime Datum
        {
            get
            {
                return this.datum;
            }
        }
    }
}
