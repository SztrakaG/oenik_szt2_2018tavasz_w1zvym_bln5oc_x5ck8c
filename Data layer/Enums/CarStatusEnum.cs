﻿// <copyright file="CarStatusEnum.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Data_Layer.Enums
{
    /// <summary>
    /// Autó stászuai ezek lehetnek
    /// </summary>
    public enum CarStatusEnum
    {
        /// <summary>
        /// elérhető autók
        /// </summary>
        elérheto,

        /// <summary>
        /// kölcsönzött autók
        /// </summary>
        kölcsönözve,

        /// <summary>
        /// eladott autók
        /// </summary>
        eladva
    }
}
