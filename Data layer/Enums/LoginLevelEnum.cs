﻿// <copyright file="LoginLevelEnum.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Data_Layer.Enums
{
    /// <summary>
    /// Bejelentkezett felhasználók szintjei
    /// </summary>
    public enum LoginLevelEnum
    {
        /// <summary>
        /// nincs hozzáférési szint
        /// </summary>
        None,

        /// <summary>
        /// alkalmazott
        /// </summary>
        Alkalmazott,

        /// <summary>
        /// vezető
        /// </summary>
        Vezeto,

        /// <summary>
        /// hozzáférés megtagadva
        /// </summary>
        AccesDenied
    }
}
