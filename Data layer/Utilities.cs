﻿// <copyright file="Utilities.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Data_Layer
{
    using System;
    using System.Security.Cryptography;
    using System.Text;
    using Data_Layer.Enums;

    /// <summary>
    /// Segéd osztály
    /// </summary>
    public abstract class Utilities
    {
        /// <summary>
        /// A bemenő string-ből jogosultság enumot képez.
        /// </summary>
        /// <param name="jogosultsag">felhasználó jogosultsága</param>
        /// <returns>bejelentkezési szint</returns>
        public static LoginLevelEnum JogosultsagToEnum(string jogosultsag)
        {
            return (LoginLevelEnum)Enum.Parse(typeof(LoginLevelEnum), jogosultsag);
        }

        /// <summary>
        /// A bemenő string-ből autó státuszt enumot képez.
        /// </summary>
        /// <param name="statusz">autó státusza</param>
        /// <returns>státusz</returns>
        public static CarStatusEnum StatusToEnum(string statusz)
        {
            return (CarStatusEnum)Enum.Parse(typeof(CarStatusEnum), statusz);
        }

        /// <summary>
        /// A bemenő string-t titkosítja
        /// </summary>
        /// <param name="text">titkosítandó szöveg</param>
        /// <returns>titkosított szöveg</returns>
        public static string GetHashSha256(string text)
        {
            byte[] bytes = Encoding.UTF8.GetBytes(text);
            SHA256Managed hashstring = new SHA256Managed();
            byte[] hash = hashstring.ComputeHash(bytes);
            string hashString = string.Empty;
            foreach (byte x in hash)
            {
                hashString += string.Format("{0:x2}", x);
            }

            return hashString;
        }
    }
}
