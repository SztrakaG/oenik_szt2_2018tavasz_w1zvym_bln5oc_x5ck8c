﻿// <copyright file="IRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Data_Layer.Interfaces
{
    using System.Linq;

    /// <summary>
    /// Összes repository interfésze
    /// </summary>
    /// <typeparam name="T">valamilyen adatbázisbeli "osztály" neve</typeparam>
    public interface IRepository<T>
    {
        /// <summary>
        /// Paraméterül kapott azonosítóval rendelkező elem törlése.
        /// </summary>
        /// <param name="toDelete">törlendő elem azoonosítója</param>
        void Delete(string toDelete);

        /// <summary>
        /// Összes elem visszadaása
        /// </summary>
        /// <returns>Összes repository elem</returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate", Justification = "A method is better here to emphasize complexity / prepare users for longer return time.")]
        IQueryable<T> GetAll();

        /// <summary>
        /// Paraméterül kapott azonosítóval rendelkező elem visszadása
        /// </summary>
        /// <param name="id">azonosító</param>
        /// <returns>Repository elem</returns>
        T GetById(string id);
    }
}
