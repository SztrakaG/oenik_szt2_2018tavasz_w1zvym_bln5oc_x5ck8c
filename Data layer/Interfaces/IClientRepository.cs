﻿// <copyright file="IClientRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Data_Layer.Interfaces
{
    /// <summary>
    /// Ügyfél repository interfésze
    /// </summary>
    /// <typeparam name="TClient">ügyfél</typeparam>
    /// <typeparam name="TContact">kapcsolattartó</typeparam>
    public interface IClientRepository<TClient, TContact> : IRepository<TClient>
    {
        /// <summary>
        /// Paraméterül kapott kliens felvétele
        /// </summary>
        /// <param name="ugyfel">ügyfél</param>
        /// <param name="kapcsolattarto">ügyfél kapcsolattartója</param>
        void Add(TClient ugyfel, TContact kapcsolattarto);
    }
}
