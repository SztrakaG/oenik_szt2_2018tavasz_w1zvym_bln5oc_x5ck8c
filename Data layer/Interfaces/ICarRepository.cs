﻿// <copyright file="ICarRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Data_Layer.Interfaces
{
    using System.Collections.Generic;
    using Enums;

    /// <summary>
    /// Autó repository interfésze
    /// </summary>
    /// <typeparam name="T">autó</typeparam>
    public interface ICarRepository<T> : IRepository<T>
        where T : Auto
    {
        /// <summary>
        /// Új autó felvétele
        /// </summary>
        /// <param name="newCar">új autó</param>
        /// <param name="brand">új autó márkája</param>
        void Add(Auto newCar, string brand);

        /// <summary>
        /// Visszaadja azon autókat amelyeknek a státusza a paraméterrel megyegyezik
        /// </summary>
        /// <param name="status">státusz</param>
        /// <returns>Autók</returns>
        IEnumerable<T> GetCarsByStatus(CarStatusEnum status);

        /// <summary>
        /// Frissíti az adatbázis az aktuális dátumnak megfelelően (kölcsönözve --> elérhető)
        /// </summary>
        void UpdateSatus();
    }
}
