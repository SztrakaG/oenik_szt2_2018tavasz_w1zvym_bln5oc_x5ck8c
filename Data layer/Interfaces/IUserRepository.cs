﻿// <copyright file="IUserRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Data_Layer.Interfaces
{
    /// <summary>
    /// FElhasználó repository interfésze
    /// </summary>
    /// <typeparam name="T">felhasználó</typeparam>
    public interface IUserRepository<T> : IRepository<T>
    {
        /// <summary>
        /// Visszaadja, hogy sikeres volt e az azonosítás
        /// </summary>
        /// <param name="username">bejelentkező felhasználóneve</param>
        /// <param name="pass">bejelentkező jelszava</param>
        /// <returns>bejelentkezés sikeressége</returns>
        bool VerifyUser(string username, string pass);
    }
}
