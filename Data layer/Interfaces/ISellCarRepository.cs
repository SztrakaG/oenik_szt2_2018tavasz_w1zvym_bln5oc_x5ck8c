﻿// <copyright file="ISellCarRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Data_Layer.Interfaces
{
    /// <summary>
    /// Eladott autó repository interfésze
    /// </summary>
    /// <typeparam name="TCar">összes autó</typeparam>
    /// <typeparam name="TBuyer">vevő</typeparam>
    /// <typeparam name="TSoldCar">eladó autó</typeparam>
    public interface ISellCarRepository<TCar, TBuyer, TSoldCar> : IRepository<TSoldCar>
    {
        /// <summary>
        /// Autó felvétele az eladott autók közé
        /// </summary>
        /// <param name="toSell">eladott autó</param>
        /// <param name="buyer">vevő</param>
        void Add(TCar toSell, TBuyer buyer);
    }
}
