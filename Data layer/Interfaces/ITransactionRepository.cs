﻿// <copyright file="ITransactionRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Data_Layer.Interfaces
{
    using System.Collections.Generic;

    /// <summary>
    /// Tranzakciók repositoryja
    /// </summary>
    public interface ITransactionRepository
    {
        /// <summary>
        /// Gets összes tranzakció
        /// </summary>
        List<Tranzakcio> Tranzakciok { get; }
    }
}
