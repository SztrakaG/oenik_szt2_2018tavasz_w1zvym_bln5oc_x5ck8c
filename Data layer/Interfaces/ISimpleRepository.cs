﻿// <copyright file="ISimpleRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Data_Layer.Interfaces
{
    /// <summary>
    /// Minden repository interfésze
    /// </summary>
    /// <typeparam name="T">valmilyen adatbázisbeli "osztály"</typeparam>
    public interface ISimpleRepository<T> : IRepository<T>
    {
        /// <summary>
        /// Paraméterül kapott elem felvétele
        /// </summary>
        /// <param name="toSave">felvételre szánt objektum</param>
        void Add(T toSave);
    }
}
