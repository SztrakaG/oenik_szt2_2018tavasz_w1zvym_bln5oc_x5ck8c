﻿// <copyright file="IRentCarRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Data_Layer.Interfaces
{
    /// <summary>
    /// Bérelt autó repository interfésze
    /// </summary>
    /// <typeparam name="TCar">összes sutó</typeparam>
    /// <typeparam name="TRenter">bérlő</typeparam>
    /// <typeparam name="TRentCar">bérlendő autó</typeparam>
    public interface IRentCarRepository<TCar, TRenter, TRentCar> : IRepository<TRentCar>
    {
        /// <summary>
        /// Kölcsönzött autó felvétele a rendszerbe
        /// </summary>
        /// <param name="toRent">kölcsönzendő autó</param>
        /// <param name="renter">kölcsönző</param>
        /// <param name="days">kölcsönzési napok száma</param>
        void Add(TCar toRent, TRenter renter, int days);
    }
}
