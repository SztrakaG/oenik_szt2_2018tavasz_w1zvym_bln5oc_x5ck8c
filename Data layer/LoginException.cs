﻿// <copyright file="LoginException.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Data_Layer
{
    using System;
    using System.Runtime.Serialization;

    /// <summary>
    /// Bejelentkezési kivételeket kezelő osztály
    /// </summary>
    [Serializable]
    public class LoginException : Exception, ISerializable
    {
        /// <summary>
        /// Kivétel üzenete
        /// </summary>
        private string msg;

        /// <summary>
        /// Kivételt okozó felhasználónév
        /// </summary>
        private string username;

        /// <summary>
        /// Initializes a new instance of the <see cref="LoginException"/> class.
        /// Konstruktor
        /// </summary>
        /// <param name="username">kivételt okozó felhasználónév</param>
        /// <param name="msg">kivétel üzenete</param>
        public LoginException(string username, string msg)
        {
            this.username = username;
            this.msg = msg;
        }

        /// <summary>
        /// Gets kivétel üzenete
        /// </summary>
        public string Msg
        {
            get
            {
                return this.msg;
            }
        }

        /// <summary>
        /// Gets kivételt okozó felhasználónév
        /// </summary>
        public string Username
        {
            get
            {
                return this.username;
            }
        }

        /// <summary>
        /// Objektum adatai
        /// </summary>
        /// <param name="info">info</param>
        /// <param name="context">context</param>
        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            base.GetObjectData(info, context);
        }
    }
}
