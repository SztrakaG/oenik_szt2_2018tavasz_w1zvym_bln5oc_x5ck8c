create table Kapcsolattarto
(
szigszam, varchar(15) not null,
kapcsolattarto_nev varchar(50) not null,
telefonszam varchar(20) not null,
email varchar(64) not null,
constraint pk_kapcsolattarto primary key(szigszam)
);

create table Ugyfel
(
adoszam varchar(20) not null,
szigszam, varchar(15) not null,
ugyfel_nev varchar(64) not null,
torzsvasarloi_pontszam numeric(2) not null,
constraint pk_kapcsolattarto primary key(szigszam),
constraint fk_ugyfel foreign key(adoszam) references Ugyfel(adoszam)
);

create table AutoTipus
(
tipus varchar(15) not null,
marka varchar(15) not null,
constraint pk_autotipus primary key(tipus)
);

create table Auto
(
rendszam varchar(10) not null,
tipus varchar(15) not null,
alapar numeric(10) not null,
szin varchar(20) not null,
kolcsonzesi_ar_1napra numeric(10) not null,
allapot varchar(15) not null,
statusz varchar(15) not null,
felveteli_datum datetime not null,
constraint pk_auto primary key(rendszam),
constraint fk_auto foreign key(tipus) references AutoTipus(tipus)
);

create table EladottAuto
(
rendszam varchar(10) not null,
adoszam varchar(20) not null,
eladasi_datum datetime not null,
eladasi_ar nuberic(10) not null,
kedvezmeny numberic(2) not null,
constraint pk_eladottauto primary key(rendszam),
constraint fk_eladottauto foreign key(rendszam) references Auto(rendszam),
constraint fk_eladottauto foreign key(adoszam) references Ugyfel(adoszam)
);

create table KolcsonzottAuto
(
kolcsonzesID numeric(5) auto_increment,
rendszam varchar(10) not null,
adoszam varchar(20) not null,
kezdeti_datum datetime not null,
kolcsonzott_napok numeric(3) not null,
kalkulalt_ar numeric(10) not null,
constraint pk_kolcsonzottauto primary key(kolcsonzesID),
constraint fk_kolcsonzottauto foreign key(rendszam) references Auto(rendszam),
constraint fk_eladottauto foreign key(adoszam) references Ugyfel(adoszam)
);

create table Felhasznalo
(
felhasznalonev varchar(20) not null,
jelszo varchar(20) not null,
jogosultsag varchar(15) not null,
aktiv bit not null,
constraint pk_felhasznalo primary key(felhasznalonev)
);