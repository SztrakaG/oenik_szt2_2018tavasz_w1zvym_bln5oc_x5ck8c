﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Data_Layer;
using Data_Layer.Enums;
using Business_Logic.Interfaces;
using System.Collections.ObjectModel;

namespace OENIK_SZT2_2018tavasz_W1ZVYM_BLN5OC_X5CK8C.ViewModel
{
    /// <summary>
    /// Felhasznalo_rogzitese_torlese_szerkesztese ViewModel osztály
    /// </summary>
    class Felhasznalo_rogzitese_torlese_szerkeszteseViewModel : Bindable
    {
        private readonly IUserManagementLogic userManagementLogic;
        private IEnumerable<Felhasznalo> felhasznalok;
        /// <summary>
        /// Felhasználókat tartalmazó lista
        /// </summary>
        public IEnumerable<Felhasznalo> Felhasznalok
        {
            get
            {
                if (userManagementLogic != null)
                {
                    felhasznalok = new ObservableCollection<Felhasznalo>(userManagementLogic.GetAll().ToList());
                }
                else
                {
                    felhasznalok = new ObservableCollection<Felhasznalo>();
                }
                return felhasznalok;
            }
            set
            {
                felhasznalok = value; OnPropertyChanged(nameof(Felhasznalok));
            }
        }
        private Felhasznalo kivalasztottFelhasznalo;
        /// <summary>
        /// Kiválasztott felhasználót tartalmazó Felhasznalo objektum
        /// </summary>
        public Felhasznalo KivalasztottFelhasznalo
        {
            get
            {
                return kivalasztottFelhasznalo;
            }
            set
            {
                kivalasztottFelhasznalo = value; OnPropertyChanged(nameof(KivalasztottFelhasznalo));
            }
        }
        /// <summary>
        /// Felhasznalo_rogzitese_torlese_szerkeszteseViewModel konstruktor
        /// </summary>
        public Felhasznalo_rogzitese_torlese_szerkeszteseViewModel(IUserManagementLogic userManagementLogic)
        {
            this.userManagementLogic = userManagementLogic;
        }
        /// <summary>
        /// Kiválasztott felhasználó értékadás metódusa
        /// </summary>
        public void KivalasztottFelhasznaloErtekadas(Felhasznalo kivalasztottfelhasznalo)
        {
            KivalasztottFelhasznalo = kivalasztottfelhasznalo;
        }
    }
}
