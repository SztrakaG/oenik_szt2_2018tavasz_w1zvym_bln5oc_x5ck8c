﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Business_Logic.Interfaces;
using System.Collections.ObjectModel;
using Data_Layer;
using Data_Layer.Enums;
using Data_Layer.Interfaces;

namespace OENIK_SZT2_2018tavasz_W1ZVYM_BLN5OC_X5CK8C.ViewModel
{
    /// <summary>
    /// Auto_felvet_torles_szerkesztes ViewModel osztály
    /// </summary>
    class Auto_felvet_torles_szerkesztesViewModel : Bindable
    {
        private readonly ICarManagementLogic carManagementLogic;
        private IEnumerable<Auto> osszesAuto;
        /// <summary>
        /// Összes autót tartalmazó lista
        /// </summary>
        public IEnumerable<Auto> OsszesAuto
        {
            get
            {
                if (carManagementLogic != null)
                {
                    osszesAuto = new ObservableCollection<Auto>(carManagementLogic.AllCars.GetAll().ToList());
                }
                else
                {
                    osszesAuto = new ObservableCollection<Auto>();
                }
                return osszesAuto;
            }
            set
            {
                osszesAuto = value; OnPropertyChanged(nameof(OsszesAuto));
            }
        }
        private Auto kivalasztottAuto;
        /// <summary>
        /// Kiválasztott autót tartalmazó Auto objektum
        /// </summary>
        public Auto KivalasztottAuto
        {
            get
            {
                return kivalasztottAuto;
            }
            set
            {
                kivalasztottAuto = value; OnPropertyChanged(nameof(KivalasztottAuto));
            }
        }
        /// <summary>
        /// Auto_felvet_torles_szerkesztesViewModel konstruktor
        /// </summary>
        public Auto_felvet_torles_szerkesztesViewModel(ICarManagementLogic carManagementLogic)
        {
            this.carManagementLogic = carManagementLogic;
        }
        public void KivalasztottAutoErtekadas(Auto kivalasztottauto)
        {
            KivalasztottAuto = kivalasztottauto;
        }
    }
}
