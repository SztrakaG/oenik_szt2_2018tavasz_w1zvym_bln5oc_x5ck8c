﻿using Business_Logic.Interfaces;
using Data_Layer;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OENIK_SZT2_2018tavasz_W1ZVYM_BLN5OC_X5CK8C.ViewModel
{
    /// <summary>
    /// Tranzakciok ViewModel osztály
    /// </summary>
    class TranzakciokViewModel : Bindable
    {
        private readonly ITransactionLogic transactionLogic;
        private IEnumerable<Tranzakcio> tranzakciok;
        /// <summary>
        /// Tranzakciókat tartalmazó lista
        /// </summary>
        public IEnumerable<Tranzakcio> Tranzakciok
        {
            get
            {
                if (transactionLogic != null)
                {
                    tranzakciok = transactionLogic.GetAllTransactions;
                }
                else
                {
                    tranzakciok = new List<Tranzakcio>();
                }
                return tranzakciok;
            }
            set
            {
                tranzakciok = value; OnPropertyChanged(nameof(Tranzakciok));
            }
        }
        /// <summary>
        /// TranzakciokViewModel konstruktor
        /// </summary>
        public TranzakciokViewModel(ITransactionLogic transactionLogic)
        {
            this.transactionLogic = transactionLogic;
        }
    }
}
