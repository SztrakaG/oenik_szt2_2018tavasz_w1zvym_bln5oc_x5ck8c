﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Business_Logic.Interfaces;
using System.Collections.ObjectModel;
using Data_Layer;
using Data_Layer.Enums;

namespace OENIK_SZT2_2018tavasz_W1ZVYM_BLN5OC_X5CK8C.ViewModel
{
    /// <summary>
    /// Auto_kolcsonzes_eladas ViewModel osztály
    /// </summary>
    class Auto_kolcsonzes_eladasViewModel : Bindable
    {
        private readonly ICarManagementLogic carManagementLogic;
        private readonly IClientManagementLogic clientManagementLogic;
        private IEnumerable<Auto> elerhetoAutok;
        /// <summary>
        /// Elérhető autókat tartalmazó lista
        /// </summary>
        public IEnumerable<Auto> ElerhetoAutok
        {
            get
            {
                if (carManagementLogic != null)
                {
                    elerhetoAutok = new ObservableCollection<Auto>(carManagementLogic.GetBySatus(CarStatusEnum.elérheto).ToList());
                }
                else
                {
                    elerhetoAutok = new ObservableCollection<Auto>();
                }
                return elerhetoAutok;
            }
            set
            {
                elerhetoAutok = value; OnPropertyChanged(nameof(ElerhetoAutok));
            }
        }
        private IEnumerable<Ugyfel> ugyfelek;
        /// <summary>
        /// Ügyfeleket tartalmazó lista
        /// </summary>
        public IEnumerable<Ugyfel> Ugyfelek
        {
            get
            {
                if (clientManagementLogic != null)
                {
                    ugyfelek = new ObservableCollection<Ugyfel>(clientManagementLogic.GetAll().ToList());
                }
                else
                {
                    ugyfelek = new ObservableCollection<Ugyfel>();
                }
                return ugyfelek;
            }
            set
            {
                ugyfelek = value; OnPropertyChanged(nameof(Ugyfelek));
            }
        }
        private Auto kivalasztottAuto;
        /// <summary>
        /// Kiválasztott autót tartalmazó Auto objektum
        /// </summary>
        public Auto KivalasztottAuto
        {
            get
            {
                return kivalasztottAuto;
            }
            set
            {
                kivalasztottAuto = value; OnPropertyChanged(nameof(KivalasztottAuto));
            }
        }
        private Ugyfel kivalasztottUgyfelEladasra;
        /// <summary>
        /// Vásárlásra kiválasztott ügyfelet (vevő) tartalmazó Ugyfel objektum
        /// </summary>
        public Ugyfel KivalasztottUgyfelEladasra
        {
            get
            {
                return kivalasztottUgyfelEladasra;
            }
            set
            {
                kivalasztottUgyfelEladasra = value; OnPropertyChanged(nameof(KivalasztottUgyfelEladasra));
            }
        }
        private Ugyfel kivalasztottUgyfelKolcsonzesre;
        /// <summary>
        /// Kölcsönzésre kiválasztott ügyfelet (kölcsönző) tartalmazó Ugyfel objektum
        /// </summary>
        public Ugyfel KivalasztottUgyfelKolcsonzesre
        {
            get
            {
                return kivalasztottUgyfelKolcsonzesre;
            }
            set
            {
                kivalasztottUgyfelKolcsonzesre = value; OnPropertyChanged(nameof(KivalasztottUgyfelKolcsonzesre));
            }
        }
        private int napokSzama;
        /// <summary>
        /// Ügyfél kölcsönzési ideje adott autóra napokban számolva int típusban tárolva
        /// </summary>
        public int NapokSzama
        {
            get
            {
                return napokSzama;
            }
            set
            {
                napokSzama = value; OnPropertyChanged(nameof(NapokSzama));
            }
        }
        private int kalkulaltArEladas;
        /// <summary>
        /// Eladásra kínált autó kalkulált ára ügyfél alapján int típusban tárolva
        /// </summary>
        public int KalkulaltArEladas
        {
            get
            {
                if (carManagementLogic != null && kivalasztottAuto != null && kivalasztottUgyfelEladasra != null)
                {
                    kalkulaltArEladas = decimal.ToInt32(carManagementLogic.CalculateCarPriceToSell(kivalasztottAuto, kivalasztottUgyfelEladasra));
                }
                else
                {
                    kalkulaltArEladas = 0;
                }
                return kalkulaltArEladas;
            }
            set
            {
                kalkulaltArEladas = value; OnPropertyChanged(nameof(KalkulaltArEladas));
            }
        }
        private int kalkulaltArKolcsonzes;
        /// <summary>
        /// Kölcsönzésre kínált autó kalkulált ára ügyfél alapján int típusban tárolva
        /// </summary>
        public int KalkulaltArKolcsonzes
        {
            get
            {
                if (carManagementLogic != null && kivalasztottAuto != null && kivalasztottUgyfelKolcsonzesre != null)
                {
                    kalkulaltArKolcsonzes = decimal.ToInt32(carManagementLogic.CalculateCarPriceToRentOut(kivalasztottAuto, kivalasztottUgyfelKolcsonzesre, napokSzama));
                }
                else
                {
                    kalkulaltArKolcsonzes = 0;
                }
                return kalkulaltArKolcsonzes;
            }
            set
            {
                kalkulaltArKolcsonzes = value; OnPropertyChanged(nameof(KalkulaltArKolcsonzes));
            }
        }
        /// <summary>
        /// Auto_kolcsonzes_eladasViewModel konstruktor
        /// </summary>
        public Auto_kolcsonzes_eladasViewModel(ICarManagementLogic carManagementLogic, IClientManagementLogic clientManagementLogic)
        {
            this.carManagementLogic = carManagementLogic;
            this.clientManagementLogic = clientManagementLogic;
        }
        /// <summary>
        /// Kiválasztott autónak értékadás metódusa
        /// </summary>
        public void KivalasztottAutoErtekadas(Auto kivalasztottauto)
        {
            KivalasztottAuto = kivalasztottauto;
        }
        /// <summary>
        /// Kiválasztott ügyfél eladásra értékadás metódusa
        /// </summary>
        public void KivalasztottUgyfelEladasraErtekadas(Ugyfel kivalasztottugyfeleladasra)
        {
            KivalasztottUgyfelEladasra = kivalasztottugyfeleladasra;
        }
        /// <summary>
        /// Kiválasztott ügyfél kölcsönzésre értékadás metódusa
        /// </summary>
        public void KivalasztottUgyfelKolcsonzesreErtekadas(Ugyfel kivalasztottugyfelkolcsonzesre)
        {
            KivalasztottUgyfelKolcsonzesre = kivalasztottugyfelkolcsonzesre;
        }
        /// <summary>
        /// Napok száma (kölcsönzési idő) értékadás metódusa
        /// </summary>
        public void NapokSzamaErtekadas(int napokszama)
        {
           NapokSzama = napokszama;
        }
    }
}
