﻿using Business_Logic.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OENIK_SZT2_2018tavasz_W1ZVYM_BLN5OC_X5CK8C.ViewModel
{
    /// <summary>
    /// Kimutatas ViewModel osztály
    /// </summary>
    class KimutatasokViewModel : Bindable
    {
        private readonly IStatementsLogic statementsLogic;
        /// <summary>
        /// Kimutatás neveinek a listája
        /// </summary>
        public List<string> KimutatasNevLista = new List<string> { "Kölcsönzött autók száma adott intervallumban", "Eladott autók száma adott intervallumban", "Eladott autókból származó bevétel adott intervallumban", "Kölcsönzött autókból származó bevétel adott intervallumban" };
        private string kimutatasEredmenye;
        /// <summary>
        /// Kimutatás eredményét tároló string
        /// </summary>
        public string KimutatasEredmenye
        {
            get
            {
                return kimutatasEredmenye;
            }
            set
            {
                kimutatasEredmenye = value; OnPropertyChanged(nameof(KimutatasEredmenye));
            }
        }
        /// <summary>
        /// KimutatasokViewModel konstruktor
        /// </summary>
        public KimutatasokViewModel(IStatementsLogic statementsLogic)
        {
            KimutatasEredmenye = "";
            this.statementsLogic = statementsLogic;
        }

        /// <summary>
        /// Kimutatás eredményének értékadás metódusa
        /// </summary>
        public void KimutatasEredmenyErtekadas(int index,DateTime from,DateTime till)
        {
            switch (KimutatasNevLista[index])
            {
                case "Kölcsönzött autók száma adott intervallumban":
                    KimutatasEredmenye = statementsLogic.RentedCarsCount(from,till).ToString();
                    break;
                case "Eladott autók száma adott intervallumban":
                    KimutatasEredmenye = statementsLogic.SoldCarsCount(from,till).ToString();
                    break;
                case "Eladott autókból származó bevétel adott intervallumban":
                    KimutatasEredmenye = statementsLogic.SoldCarsIncome(from, till).ToString();
                    break;
                case "Kölcsönzött autókból származó bevétel adott intervallumban":
                    KimutatasEredmenye = statementsLogic.RentedCarsIncome(from, till).ToString();
                    break;
                default:
                    break;
            }
        }
    }
}
