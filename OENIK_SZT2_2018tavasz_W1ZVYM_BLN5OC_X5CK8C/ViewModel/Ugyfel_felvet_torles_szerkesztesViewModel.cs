﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Data_Layer;
using Data_Layer.Enums;
using Business_Logic.Interfaces;
using System.Collections.ObjectModel;

namespace OENIK_SZT2_2018tavasz_W1ZVYM_BLN5OC_X5CK8C.ViewModel
{
    /// <summary>
    /// Ugyfel_felvet_torles_szerkesztes ViewModel osztály
    /// </summary>
    class Ugyfel_felvet_torles_szerkesztesViewModel : Bindable
    {
        private readonly IClientManagementLogic clientManagementLogic;
        private IEnumerable<Ugyfel> ugyfelek;
        /// <summary>
        /// Ugyfeleket tartalmazó lista
        /// </summary>
        public IEnumerable<Ugyfel> Ugyfelek
        {
            get
            {
                if (clientManagementLogic != null)
                {
                    ugyfelek = new ObservableCollection<Ugyfel>(clientManagementLogic.GetAll().ToList());
                }
                else
                {
                    ugyfelek = new ObservableCollection<Ugyfel>();
                }
                return ugyfelek;
            }
            set
            {
                ugyfelek = value; OnPropertyChanged(nameof(Ugyfelek));
            }
        }
        private Ugyfel kivalasztottUgyfel;
        /// <summary>
        /// Kiválasztott ügyfelet tartalmazó Ugyfel objektum
        /// </summary>
        public Ugyfel KivalasztottUgyfel
        {
            get
            {
                return kivalasztottUgyfel;
            }
            set
            {
                kivalasztottUgyfel = value; OnPropertyChanged(nameof(KivalasztottUgyfel));
            }
        }
        /// <summary>
        /// Ugyfel_felvet_torles_szerkesztesViewModel konstruktor
        /// </summary>
        public Ugyfel_felvet_torles_szerkesztesViewModel(IClientManagementLogic clientManagementLogic)
        {
            this.clientManagementLogic = clientManagementLogic;
        }
        /// <summary>
        /// Kiválasztott ügyfél értékadás metódus
        /// </summary>
        public void KivalasztottUgyfelErtekadas(Ugyfel kivalasztottugyfel)
        {
            KivalasztottUgyfel = kivalasztottugyfel;
        }
    }
}
