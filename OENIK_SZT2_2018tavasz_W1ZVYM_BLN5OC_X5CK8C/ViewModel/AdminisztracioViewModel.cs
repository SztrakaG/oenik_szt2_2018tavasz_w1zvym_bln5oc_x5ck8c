﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Data_Layer;
using Data_Layer.Enums;
using Business_Logic.Interfaces;
using System.Windows;

namespace OENIK_SZT2_2018tavasz_W1ZVYM_BLN5OC_X5CK8C.ViewModel
{
    /// <summary>
    /// Adminisztracio ViewModel osztály
    /// </summary>
    class AdminisztracioViewModel : Bindable
    {
        private readonly IUserManagementLogic userManagementLogic;
        private Visibility felhasznaloiJogosultsag;
        /// <summary>
        /// Felhasználó láthatósági jogát tároló Visibility tulajdonság
        /// </summary>
        public Visibility FelhasznaloiJogosultsag
        {
            get
            {
                return felhasznaloiJogosultsag;
            }
            set
            {
                felhasznaloiJogosultsag = value;OnPropertyChanged(nameof(FelhasznaloiJogosultsag));
            }
        }
        private string felhasznalonev;
        /// <summary>
        /// Felhasználó felhasználónevét tároló string
        /// </summary>
        public string Felhasznalonev
        {
            get
            {
                return felhasznalonev;
            }
            set
            {
                felhasznalonev = value; OnPropertyChanged(nameof(Felhasznalonev));
            }
        }
        /// <summary>
        /// AdminisztracioViewModel konstruktor
        /// </summary>
        public AdminisztracioViewModel(IUserManagementLogic userManagementLogic, string Felhasznalonev)
        {
            this.Felhasznalonev = Felhasznalonev;
            this.userManagementLogic = userManagementLogic;
            FelhasznaloiJogosultsag = this.userManagementLogic.GetById(Felhasznalonev).jogosultsag == LoginLevelEnum.Vezeto.ToString() ? Visibility.Visible : Visibility.Hidden;
        }
    }
}
