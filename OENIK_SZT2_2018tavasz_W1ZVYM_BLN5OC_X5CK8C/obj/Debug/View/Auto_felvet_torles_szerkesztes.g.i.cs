﻿#pragma checksum "..\..\..\View\Auto_felvet_torles_szerkesztes.xaml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "3E28E9A3C0DCC5828572D0EBD64B45E36F0BBFC4"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using OENIK_SZT2_2018tavasz_W1ZVYM_BLN5OC_X5CK8C;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace OENIK_SZT2_2018tavasz_W1ZVYM_BLN5OC_X5CK8C {
    
    
    /// <summary>
    /// Auto_Felvet_Torles_Szerkesztes
    /// </summary>
    public partial class Auto_Felvet_Torles_Szerkesztes : System.Windows.Window, System.Windows.Markup.IComponentConnector {
        
        
        #line 18 "..\..\..\View\Auto_felvet_torles_szerkesztes.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox RendszamFelvetTextBox;
        
        #line default
        #line hidden
        
        
        #line 19 "..\..\..\View\Auto_felvet_torles_szerkesztes.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox SzinFelvetTextBox;
        
        #line default
        #line hidden
        
        
        #line 20 "..\..\..\View\Auto_felvet_torles_szerkesztes.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox MarkaFelvetTextBox;
        
        #line default
        #line hidden
        
        
        #line 21 "..\..\..\View\Auto_felvet_torles_szerkesztes.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox ArFelvetTextBox;
        
        #line default
        #line hidden
        
        
        #line 22 "..\..\..\View\Auto_felvet_torles_szerkesztes.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button UjAutoHozzaadasButton;
        
        #line default
        #line hidden
        
        
        #line 24 "..\..\..\View\Auto_felvet_torles_szerkesztes.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox KolcsonzesiArFelvetTextBox;
        
        #line default
        #line hidden
        
        
        #line 26 "..\..\..\View\Auto_felvet_torles_szerkesztes.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox AllapotFelvetTextBox;
        
        #line default
        #line hidden
        
        
        #line 28 "..\..\..\View\Auto_felvet_torles_szerkesztes.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox TipusFelvetTextBox;
        
        #line default
        #line hidden
        
        
        #line 38 "..\..\..\View\Auto_felvet_torles_szerkesztes.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox RendszamSzerkesztTextBox;
        
        #line default
        #line hidden
        
        
        #line 39 "..\..\..\View\Auto_felvet_torles_szerkesztes.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox SzinSzerkesztTextBox;
        
        #line default
        #line hidden
        
        
        #line 40 "..\..\..\View\Auto_felvet_torles_szerkesztes.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox MarkaSzerkesztTextBox;
        
        #line default
        #line hidden
        
        
        #line 41 "..\..\..\View\Auto_felvet_torles_szerkesztes.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox ArSzerkesztTextBox;
        
        #line default
        #line hidden
        
        
        #line 42 "..\..\..\View\Auto_felvet_torles_szerkesztes.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button KivalasztottAutoSzerkesztButton;
        
        #line default
        #line hidden
        
        
        #line 44 "..\..\..\View\Auto_felvet_torles_szerkesztes.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox KolcsonzesiArSzerkesztTextBox;
        
        #line default
        #line hidden
        
        
        #line 46 "..\..\..\View\Auto_felvet_torles_szerkesztes.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox AllapotSzerkesztTextBox;
        
        #line default
        #line hidden
        
        
        #line 48 "..\..\..\View\Auto_felvet_torles_szerkesztes.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox TipusSzerkesztTextBox;
        
        #line default
        #line hidden
        
        
        #line 52 "..\..\..\View\Auto_felvet_torles_szerkesztes.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ListBox AutoListBox;
        
        #line default
        #line hidden
        
        
        #line 54 "..\..\..\View\Auto_felvet_torles_szerkesztes.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button AutoTorlesButton;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/OENIK_SZT2_2018tavasz_W1ZVYM_BLN5OC_X5CK8C;component/view/auto_felvet_torles_sze" +
                    "rkesztes.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\..\View\Auto_felvet_torles_szerkesztes.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            
            #line 8 "..\..\..\View\Auto_felvet_torles_szerkesztes.xaml"
            ((OENIK_SZT2_2018tavasz_W1ZVYM_BLN5OC_X5CK8C.Auto_Felvet_Torles_Szerkesztes)(target)).Loaded += new System.Windows.RoutedEventHandler(this.Window_Loaded);
            
            #line default
            #line hidden
            return;
            case 2:
            this.RendszamFelvetTextBox = ((System.Windows.Controls.TextBox)(target));
            return;
            case 3:
            this.SzinFelvetTextBox = ((System.Windows.Controls.TextBox)(target));
            return;
            case 4:
            this.MarkaFelvetTextBox = ((System.Windows.Controls.TextBox)(target));
            return;
            case 5:
            this.ArFelvetTextBox = ((System.Windows.Controls.TextBox)(target));
            return;
            case 6:
            this.UjAutoHozzaadasButton = ((System.Windows.Controls.Button)(target));
            
            #line 22 "..\..\..\View\Auto_felvet_torles_szerkesztes.xaml"
            this.UjAutoHozzaadasButton.Click += new System.Windows.RoutedEventHandler(this.UjAutoHozzaadasButton_Click);
            
            #line default
            #line hidden
            return;
            case 7:
            this.KolcsonzesiArFelvetTextBox = ((System.Windows.Controls.TextBox)(target));
            return;
            case 8:
            this.AllapotFelvetTextBox = ((System.Windows.Controls.TextBox)(target));
            return;
            case 9:
            this.TipusFelvetTextBox = ((System.Windows.Controls.TextBox)(target));
            return;
            case 10:
            this.RendszamSzerkesztTextBox = ((System.Windows.Controls.TextBox)(target));
            return;
            case 11:
            this.SzinSzerkesztTextBox = ((System.Windows.Controls.TextBox)(target));
            return;
            case 12:
            this.MarkaSzerkesztTextBox = ((System.Windows.Controls.TextBox)(target));
            return;
            case 13:
            this.ArSzerkesztTextBox = ((System.Windows.Controls.TextBox)(target));
            return;
            case 14:
            this.KivalasztottAutoSzerkesztButton = ((System.Windows.Controls.Button)(target));
            
            #line 42 "..\..\..\View\Auto_felvet_torles_szerkesztes.xaml"
            this.KivalasztottAutoSzerkesztButton.Click += new System.Windows.RoutedEventHandler(this.KivalasztottAutoSzerkesztButton_Click);
            
            #line default
            #line hidden
            return;
            case 15:
            this.KolcsonzesiArSzerkesztTextBox = ((System.Windows.Controls.TextBox)(target));
            return;
            case 16:
            this.AllapotSzerkesztTextBox = ((System.Windows.Controls.TextBox)(target));
            return;
            case 17:
            this.TipusSzerkesztTextBox = ((System.Windows.Controls.TextBox)(target));
            return;
            case 18:
            this.AutoListBox = ((System.Windows.Controls.ListBox)(target));
            
            #line 52 "..\..\..\View\Auto_felvet_torles_szerkesztes.xaml"
            this.AutoListBox.SelectionChanged += new System.Windows.Controls.SelectionChangedEventHandler(this.AutoListBox_SelectionChanged);
            
            #line default
            #line hidden
            return;
            case 19:
            this.AutoTorlesButton = ((System.Windows.Controls.Button)(target));
            
            #line 54 "..\..\..\View\Auto_felvet_torles_szerkesztes.xaml"
            this.AutoTorlesButton.Click += new System.Windows.RoutedEventHandler(this.AutoTorlesButton_Click);
            
            #line default
            #line hidden
            return;
            }
            this._contentLoaded = true;
        }
    }
}

