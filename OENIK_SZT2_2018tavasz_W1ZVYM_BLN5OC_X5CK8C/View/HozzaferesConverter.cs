﻿using Data_Layer;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace OENIK_SZT2_2018tavasz_W1ZVYM_BLN5OC_X5CK8C.View
{
    /// <summary>
    /// Felhasználóból számot konvertáló IValueConverter osztály
    /// </summary>
    class HozzaferesConverter : IValueConverter
    {
        /// <summary>
        /// Convert függvény
        /// </summary>
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            Felhasznalo felhasznalo = (Felhasznalo)value;
            if (felhasznalo != null)
            {
                switch (felhasznalo.jogosultsag)
                {
                    case "None":
                        return 0;
                    case "Alkalmazott":
                        return 1;
                    case "Vezeto":
                        return 2;
                    case "AccesDenied":
                        return 3;
                    default:
                        return 0;
                }
            }
            else
            {
                return 0;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
