﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Business_Logic.Interfaces;
using Business_Logic;
using Data_Layer;
using Data_Layer.Enums;

namespace OENIK_SZT2_2018tavasz_W1ZVYM_BLN5OC_X5CK8C
{
    /// <summary>
    /// Interaction logic for Felhasznalo_rogzitese_torlese_szerkesztese.xaml
    /// </summary>
    public partial class Felhasznalo_Rogzitese_Torlese_Szerkesztese : Window
    {
        private AdatbazisEntities adatbazisEntities;
        private ViewModel.Felhasznalo_rogzitese_torlese_szerkeszteseViewModel viewModel;
        private IUserManagementLogic userManagementLogic;
        /// <summary>
        /// Felhasznalo_Rogzitese_Torlese_Szerkesztese konstruktor
        /// </summary>
        public Felhasznalo_Rogzitese_Torlese_Szerkesztese(AdatbazisEntities adatbazisEntities)
        {
            this.adatbazisEntities = adatbazisEntities;
            userManagementLogic = new UserManagementLogic(this.adatbazisEntities);
            viewModel = new ViewModel.Felhasznalo_rogzitese_torlese_szerkeszteseViewModel(userManagementLogic);
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            HozzaferesRogzitComboBox.ItemsSource = Enum.GetValues(typeof(LoginLevelEnum));
            HozzaferesSzerkesztComboBox.ItemsSource = Enum.GetValues(typeof(LoginLevelEnum));
            this.DataContext = viewModel;
        }
        private void UIRefresh()
        {
            FelhasznaloListBox.GetBindingExpression(ListBox.ItemsSourceProperty).UpdateTarget();
        }

        private void FelhasznaloTorlesButton_Click(object sender, RoutedEventArgs e)
        {
            if (viewModel.KivalasztottFelhasznalo != null)
            {
                if (MessageBox.Show("Biztosan törli a " + viewModel.KivalasztottFelhasznalo.felhasznalonev + " nevű felhasználót?", "Question", MessageBoxButton.YesNo, MessageBoxImage.Warning) == MessageBoxResult.Yes)
                {
                    userManagementLogic.Delete(viewModel.KivalasztottFelhasznalo.felhasznalonev);
                    UIRefresh();
                }
            }
            else
            {
                MessageBox.Show("Nincs kiválasztva felhasználó!");
            }
        }

        private void FelhasznaloSzerkesztButton_Click(object sender, RoutedEventArgs e)
        {
            UIRefresh();
        }

        private void UjFelhasznaloButton_Click(object sender, RoutedEventArgs e)
        {
            if (MessageBox.Show("Biztosan hozzáadja a " + FelhasznalonevRogzitTextBox.Text + " nevű felhasználót?", "Question", MessageBoxButton.YesNo, MessageBoxImage.Warning) == MessageBoxResult.Yes)
            {
                Felhasznalo ujFelhasznalo = new Felhasznalo()
                {
                    felhasznalonev = FelhasznalonevRogzitTextBox.Text,
                    jelszo = JelszoRogzitTextBox.Text,
                    jogosultsag = HozzaferesRogzitComboBox.SelectedValue.ToString(),
                    aktiv = AktivRogzitCheckBox.IsChecked.Value
                };
                try
                {
                    userManagementLogic.Add(ujFelhasznalo);
                    UIRefresh();
                    FelhasznalonevRogzitTextBox.Text = "";
                    JelszoRogzitTextBox.Text = "";
                    HozzaferesRogzitComboBox.SelectedIndex = 0;
                    AktivRogzitCheckBox.IsChecked = false;
                }
                catch (InvalidOperationException ex)
                {

                    MessageBox.Show(ex.Message);
                }
            }
        }

        private void FelhasznaloListBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (FelhasznaloListBox.SelectedIndex > -1)
            {
                viewModel.KivalasztottFelhasznaloErtekadas((Felhasznalo)FelhasznaloListBox.SelectedItem);
            }
            else
            {
                viewModel.KivalasztottFelhasznaloErtekadas(null);
            }
            UIRefresh();
        }
    }
}
