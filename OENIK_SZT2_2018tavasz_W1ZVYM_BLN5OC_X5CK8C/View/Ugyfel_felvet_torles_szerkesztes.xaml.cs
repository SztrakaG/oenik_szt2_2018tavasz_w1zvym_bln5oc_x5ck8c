﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Business_Logic.Interfaces;
using Business_Logic;
using Data_Layer;

namespace OENIK_SZT2_2018tavasz_W1ZVYM_BLN5OC_X5CK8C
{
    /// <summary>
    /// Interaction logic for Ugyfel_felvet_torles_szerkesztes.xaml
    /// </summary>
    public partial class Ugyfel_Felvet_Torles_Szerkesztes : Window
    {
        private AdatbazisEntities adatbazisEntities;
        private ViewModel.Ugyfel_felvet_torles_szerkesztesViewModel viewModel;
        private IClientManagementLogic clientManagementLogic;
        /// <summary>
        /// Ugyfel_Felvet_Torles_Szerkesztes konstruktor
        /// </summary>
        public Ugyfel_Felvet_Torles_Szerkesztes(AdatbazisEntities adatbazisEntities)
        {
            this.adatbazisEntities = adatbazisEntities;
            clientManagementLogic = new ClientManagementLogic(this.adatbazisEntities);
            viewModel = new ViewModel.Ugyfel_felvet_torles_szerkesztesViewModel(clientManagementLogic);
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            this.DataContext = viewModel;
        }
        private void UIRefresh()
        {
            UgyfelListBox.GetBindingExpression(ListBox.ItemsSourceProperty).UpdateTarget();
        }

        private void UjUgyfelButton_Click(object sender, RoutedEventArgs e)
        {
            if (MessageBox.Show("Biztosan hozzáadja a " + UgyfelNevFelvetelTextBox.Text + " nevű ügyfelet?", "Question", MessageBoxButton.YesNo, MessageBoxImage.Warning) == MessageBoxResult.Yes)
            {
                Kapcsolattarto ujKapcsolattarto = new Kapcsolattarto()
                {
                    szigszam = KapcsolattartoSzigszamFelvetelTextBox.Text,
                    kapcsolattarto_nev = KapcsolattartoNevFelvetelTextBox.Text,
                    telefonszam = KapcsolattartoTelefonszamFelvetelTextBox.Text,
                    email = KapcsolattartoEmailFelvetelTextBox.Text
                };
                Ugyfel ujUgyfel = new Ugyfel()
                {
                    ugyfel_nev = UgyfelNevFelvetelTextBox.Text,
                    adoszam = UgyfelAdoszamFelvetelTextBox.Text,
                    szigszam = KapcsolattartoSzigszamFelvetelTextBox.Text,
                    torzsvasarloi_pontszam = 0,
                };
                try
                {
                    clientManagementLogic.Add(ujUgyfel, ujKapcsolattarto);
                    UIRefresh();
                    UgyfelNevFelvetelTextBox.Text = "";
                    UgyfelAdoszamFelvetelTextBox.Text = "";
                    KapcsolattartoEmailFelvetelTextBox.Text = "";
                    KapcsolattartoNevFelvetelTextBox.Text = "";
                    KapcsolattartoSzigszamFelvetelTextBox.Text = "";
                    KapcsolattartoTelefonszamFelvetelTextBox.Text = "";
                }
                catch (InvalidOperationException ex)
                {

                    MessageBox.Show(ex.Message);
                }
            }
        }

        private void UgyfelSzerkesztButton_Click(object sender, RoutedEventArgs e)
        {
            UIRefresh();
        }

        private void UgyfelTorlesButton_Click(object sender, RoutedEventArgs e)
        {
            if (viewModel.KivalasztottUgyfel != null)
            {
                if (MessageBox.Show("Biztosan törli a " + viewModel.KivalasztottUgyfel.ugyfel_nev + " nevű ügyfelet?", "Question", MessageBoxButton.YesNo, MessageBoxImage.Warning) == MessageBoxResult.Yes)
                {
                    try
                    {
                        clientManagementLogic.Delete(viewModel.KivalasztottUgyfel.adoszam);
                    }
                    catch (InvalidOperationException ex)
                    {
                        MessageBox.Show(ex.Message);
                    }
                    finally
                    {
                        UIRefresh();
                    }
                }
            }
            else
            {
                MessageBox.Show("Nincs kiválasztva ügyfél!");
            }
        }

        private void UgyfelListBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (UgyfelListBox.SelectedIndex > -1)
            {
                viewModel.KivalasztottUgyfelErtekadas((Ugyfel)UgyfelListBox.SelectedItem);
            }
            else
            {
                viewModel.KivalasztottUgyfelErtekadas(null);
            }
        }
    }
}
