﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Business_Logic.Interfaces;
using Business_Logic;
using Data_Layer;

namespace OENIK_SZT2_2018tavasz_W1ZVYM_BLN5OC_X5CK8C
{
    /// <summary>
    /// Interaction logic for Auto_kolcsonzes_eladas.xaml
    /// </summary>
    public partial class Auto_Kolcsonzes_Eladas : Window
    {
        private AdatbazisEntities adatbazisEntities;
        private ViewModel.Auto_kolcsonzes_eladasViewModel viewModel;
        private ICarManagementLogic carManagementLogic;
        private IClientManagementLogic clientManagementLogic;
        /// <summary>
        /// Auto_Kolcsonzes_Eladas konstruktor
        /// </summary>
        public Auto_Kolcsonzes_Eladas(AdatbazisEntities adatbazisEntities)
        {
            this.adatbazisEntities = adatbazisEntities;
            carManagementLogic = new CarManagementLogic(this.adatbazisEntities);
            clientManagementLogic = new ClientManagementLogic(this.adatbazisEntities);
            viewModel = new ViewModel.Auto_kolcsonzes_eladasViewModel(carManagementLogic,clientManagementLogic);
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            this.DataContext = viewModel;
        }

        private void UIRefresh()
        {
            AutolistaListBox.GetBindingExpression(ListBox.ItemsSourceProperty).UpdateTarget();
            KalkulaltArLabel.GetBindingExpression(Label.ContentProperty).UpdateTarget();
            if (KalkulaltAr2Label != null)
            {
                KalkulaltAr2Label.GetBindingExpression(Label.ContentProperty).UpdateTarget();
            }
        }

        private void AutolistaListBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (AutolistaListBox.SelectedIndex > -1)
            {
                viewModel.KivalasztottAutoErtekadas((Auto)AutolistaListBox.SelectedItem);
            }
            else
            {
                viewModel.KivalasztottAutoErtekadas(null);
            }
            UIRefresh();
        }

        private void UgyfelComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (UgyfelComboBox.SelectedIndex > -1)
            {
                viewModel.KivalasztottUgyfelKolcsonzesreErtekadas((Ugyfel)UgyfelComboBox.SelectedItem);
            }
            else
            {
                viewModel.KivalasztottUgyfelKolcsonzesreErtekadas(null);
            }
            UIRefresh();
        }
        private void UgyfelComboBox2_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (UgyfelComboBox2.SelectedIndex > -1)
            {
                viewModel.KivalasztottUgyfelEladasraErtekadas((Ugyfel)UgyfelComboBox2.SelectedItem);
            }
            else
            {
                viewModel.KivalasztottUgyfelEladasraErtekadas(null);
            }
            UIRefresh();
        }

        private void MeddigDatePicker_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            if(MeddigDatePicker.SelectedDate.Value.Year == DateTime.Now.Year && MeddigDatePicker.SelectedDate.Value.Month == DateTime.Now.Month && MeddigDatePicker.SelectedDate.Value.Day == DateTime.Now.Day)
            {
                viewModel.NapokSzamaErtekadas((int)((MeddigDatePicker.SelectedDate.Value - DateTime.Now).Days) + 1);
            }
            else
            {
                viewModel.NapokSzamaErtekadas((int)((MeddigDatePicker.SelectedDate.Value - DateTime.Now).Days) + 2);
            }
            UIRefresh();
        }

        private void KolcsonzesButton_Click(object sender, RoutedEventArgs e)
        {
            if (viewModel.KivalasztottAuto != null)
            {
                if (MessageBox.Show("Biztosan kölcsön adja a " + viewModel.KivalasztottAuto.rendszam + " rendszámú autót " + viewModel.KivalasztottUgyfelKolcsonzesre.ugyfel_nev + " nevű ügyfélnek?", "Question", MessageBoxButton.YesNo, MessageBoxImage.Warning) == MessageBoxResult.Yes)
                {
                    carManagementLogic.RentOutCar(viewModel.KivalasztottAuto, viewModel.KivalasztottUgyfelKolcsonzesre, viewModel.NapokSzama);
                    UIRefresh();
                }
            }
            else
            {
                MessageBox.Show("Nincs kiválasztva autó. Válasszon ki egy autót!");
            }
        }

        private void EladasButton_Click(object sender, RoutedEventArgs e)
        {
            if (viewModel.KivalasztottAuto != null)
            {
                if (MessageBox.Show("Biztosan eladja a " + viewModel.KivalasztottAuto.rendszam + " rendszámú autót " + viewModel.KivalasztottUgyfelEladasra.ugyfel_nev + " nevű ügyfélnek?", "Question", MessageBoxButton.YesNo, MessageBoxImage.Warning) == MessageBoxResult.Yes)
                {
                    carManagementLogic.SellCar(viewModel.KivalasztottAuto, viewModel.KivalasztottUgyfelEladasra);
                    UIRefresh();
                }
            }
            else
            {
                MessageBox.Show("Nincs kiválasztva autó. Válasszon ki egy autót!");
            }
        }
    }
}
