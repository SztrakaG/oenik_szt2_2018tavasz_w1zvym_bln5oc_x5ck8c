﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Business_Logic.Interfaces;
using Business_Logic;
using Data_Layer;
using Data_Layer.Enums;

namespace OENIK_SZT2_2018tavasz_W1ZVYM_BLN5OC_X5CK8C
{
    /// <summary>
    /// Interaction logic for Auto_felvet_torles_szerkesztes.xaml
    /// </summary>
    public partial class Auto_Felvet_Torles_Szerkesztes : Window
    {
        private AdatbazisEntities adatbazisEntities;
        private ViewModel.Auto_felvet_torles_szerkesztesViewModel viewModel;
        private ICarManagementLogic carManagementLogic;
        /// <summary>
        /// Auto_Felvet_Torles_Szerkesztes konstruktor
        /// </summary>
        public Auto_Felvet_Torles_Szerkesztes(AdatbazisEntities adatbazisEntities)
        {
            this.adatbazisEntities = adatbazisEntities;
            carManagementLogic = new CarManagementLogic(this.adatbazisEntities);
            viewModel = new ViewModel.Auto_felvet_torles_szerkesztesViewModel(carManagementLogic);
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            this.DataContext = viewModel;
        }

        private void UIRefresh()
        {
            AutoListBox.GetBindingExpression(ListBox.ItemsSourceProperty).UpdateTarget();
        }

        private void AutoTorlesButton_Click(object sender, RoutedEventArgs e)
        {
            if (viewModel.KivalasztottAuto != null)
            {
                if (MessageBox.Show("Biztosan törli a " + viewModel.KivalasztottAuto.rendszam + " rendszámú autót?", "Question", MessageBoxButton.YesNo, MessageBoxImage.Warning) == MessageBoxResult.Yes)
                {
                    try
                    {
                        carManagementLogic.Delete(viewModel.KivalasztottAuto.rendszam);
                    }
                    catch (InvalidOperationException ex)
                    {
                        MessageBox.Show(ex.Message);
                    }
                    finally
                    {
                        UIRefresh();
                    }
                }
            }
            else
            {
                MessageBox.Show("Nincs kiválasztva autó!");
            }
        }

        private void UjAutoHozzaadasButton_Click(object sender, RoutedEventArgs e)
        {

            if (MessageBox.Show("Biztosan hozzáadja a " + RendszamFelvetTextBox.Text + " rendszámú autót?", "Question", MessageBoxButton.YesNo, MessageBoxImage.Warning) == MessageBoxResult.Yes)
            {
                Auto ujAuto = new Auto()
                {
                    rendszam = RendszamFelvetTextBox.Text,
                    alapar = decimal.Parse(ArFelvetTextBox.Text),
                    szin = SzinFelvetTextBox.Text,
                    kolcsonzesi_ar_1napra = decimal.Parse(KolcsonzesiArFelvetTextBox.Text),
                    allapot = AllapotFelvetTextBox.Text,
                    tipus = TipusFelvetTextBox.Text,
                    statusz = CarStatusEnum.elérheto.ToString(),
                    felveteli_datum = DateTime.Now
                };
                try
                {
                    carManagementLogic.Add(ujAuto, MarkaFelvetTextBox.Text);
                    UIRefresh();
                    RendszamFelvetTextBox.Text = "";
                    ArFelvetTextBox.Text = "";
                    MarkaFelvetTextBox.Text = "";
                    SzinFelvetTextBox.Text = "";
                    KolcsonzesiArFelvetTextBox.Text = "";
                    AllapotFelvetTextBox.Text = "";
                    TipusFelvetTextBox.Text = "";
                }
                catch (InvalidOperationException ex)
                {

                    MessageBox.Show(ex.Message);
                }
            }
        }

        private void KivalasztottAutoSzerkesztButton_Click(object sender, RoutedEventArgs e)
        {
            UIRefresh();
        }

        private void AutoListBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (AutoListBox.SelectedIndex > -1)
            {
                viewModel.KivalasztottAutoErtekadas((Auto)AutoListBox.SelectedItem);
            }
            else
            {
                viewModel.KivalasztottAutoErtekadas(null);
            }
        }
    }
}
