﻿using Business_Logic.Interfaces;
using Business_Logic.Logic;
using Data_Layer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace OENIK_SZT2_2018tavasz_W1ZVYM_BLN5OC_X5CK8C
{
    /// <summary>
    /// Interaction logic for Kimutatasok.xaml
    /// </summary>
    public partial class Kimutatasok : Window
    {
        private AdatbazisEntities adatbazisEntities;
        private ViewModel.KimutatasokViewModel viewModel;
        private IStatementsLogic statementsLogic;
        /// <summary>
        /// Kimutatasok konstruktor
        /// </summary>
        public Kimutatasok(AdatbazisEntities adatbazisEntities)
        {
            this.adatbazisEntities = adatbazisEntities;
            statementsLogic = new StatementsLogic(this.adatbazisEntities);
            viewModel = new ViewModel.KimutatasokViewModel(statementsLogic);
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            KimutatasNevComboBox.Items.Add("Kölcsönzött autók száma adott intervallumban");
            KimutatasNevComboBox.Items.Add("Eladott autók száma adott intervallumban");
            KimutatasNevComboBox.Items.Add("Eladott autókból származó bevétel adott intervallumban");
            KimutatasNevComboBox.Items.Add("Kölcsönzött autókból származó bevétel adott intervallumban");
            this.DataContext = viewModel;
        }

        private void KimutatasKivalasztasaButton_Click(object sender, RoutedEventArgs e)
        {
            if (MettolDatePicker.SelectedDate <= MeddigDatePicker.SelectedDate)
            {
                viewModel.KimutatasEredmenyErtekadas(KimutatasNevComboBox.SelectedIndex, (DateTime)MettolDatePicker.SelectedDate, (DateTime)MeddigDatePicker.SelectedDate);
            }
            else
            {
                MessageBox.Show("A végdátumnak nem lehet kisebb a kezdeti dátumnál");
            }
        }
    }
}
