﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Business_Logic.Interfaces;
using Business_Logic;
using Data_Layer;
using Business_Logic.Logic;

namespace OENIK_SZT2_2018tavasz_W1ZVYM_BLN5OC_X5CK8C
{
    /// <summary>
    /// Interaction logic for Adminisztracio_tulajdonos.xaml
    /// </summary>
    public partial class Adminisztracio : Window
    {
        private AdatbazisEntities adatbazisEntities;
        private ViewModel.AdminisztracioViewModel viewModel;
        private IUserManagementLogic userManagementLogic;
        /// <summary>
        /// Adminisztráció konstruktor
        /// </summary>
        public Adminisztracio(string felhasznalonev, AdatbazisEntities adatbazisEntities)
        {
            this.adatbazisEntities = adatbazisEntities;
            userManagementLogic = new UserManagementLogic(adatbazisEntities);
            viewModel = new ViewModel.AdminisztracioViewModel(userManagementLogic,felhasznalonev);
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            this.DataContext = viewModel;
        }
        private void KijelentkezesButton_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void AutoAdminisztracioButton_Click(object sender, RoutedEventArgs e)
        {
            Auto_Felvet_Torles_Szerkesztes AutoAdminisztracioAblak = new Auto_Felvet_Torles_Szerkesztes(adatbazisEntities);
            AutoAdminisztracioAblak.ShowDialog();
        }

        private void AutoKolcsonEladasButton_Click(object sender, RoutedEventArgs e)
        {
            Auto_Kolcsonzes_Eladas AutoKolcsonzesEladasAblak = new Auto_Kolcsonzes_Eladas(adatbazisEntities);
            AutoKolcsonzesEladasAblak.ShowDialog();
        }

        private void UgyfelAdminisztracioButton_Click(object sender, RoutedEventArgs e)
        {
            Ugyfel_Felvet_Torles_Szerkesztes UgyfelAdminisztracioAblak = new Ugyfel_Felvet_Torles_Szerkesztes(adatbazisEntities);
            UgyfelAdminisztracioAblak.ShowDialog();
        }

        private void FelhasznaloAdminisztracioButton_Click(object sender, RoutedEventArgs e)
        {
            Felhasznalo_Rogzitese_Torlese_Szerkesztese FelhasznaloAdminisztracioAblak = new Felhasznalo_Rogzitese_Torlese_Szerkesztese(adatbazisEntities);
            FelhasznaloAdminisztracioAblak.ShowDialog();
        }

        private void TranzakciokButton_Click(object sender, RoutedEventArgs e)
        {
            Tranzakciok TranzakciokAblak = new Tranzakciok(adatbazisEntities);
            TranzakciokAblak.ShowDialog();
        }

        private void KimutatasokButton_Click(object sender, RoutedEventArgs e)
        {
            Kimutatasok KimutatasokAblak = new Kimutatasok(adatbazisEntities);
            KimutatasokAblak.ShowDialog();
        }
    }
}
