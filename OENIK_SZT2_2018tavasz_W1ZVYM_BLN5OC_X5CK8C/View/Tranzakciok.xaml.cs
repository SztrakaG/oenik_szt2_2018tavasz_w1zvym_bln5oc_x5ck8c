﻿using Business_Logic;
using Business_Logic.Interfaces;
using Data_Layer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace OENIK_SZT2_2018tavasz_W1ZVYM_BLN5OC_X5CK8C
{
    /// <summary>
    /// Interaction logic for Tranzakciok.xaml
    /// </summary>
    public partial class Tranzakciok : Window
    {
        private AdatbazisEntities adatbazisEntities;
        private ViewModel.TranzakciokViewModel viewModel;
        private ITransactionLogic transactionLogic;
        /// <summary>
        /// Tranzakciok konstruktor
        /// </summary>
        public Tranzakciok(AdatbazisEntities adatbazisEntities)
        {
            this.adatbazisEntities = adatbazisEntities;
            transactionLogic = new TransactionLogic(this.adatbazisEntities);
            viewModel = new ViewModel.TranzakciokViewModel(transactionLogic);
            InitializeComponent();
            UIRefresh();
        }
        private void UIRefresh()
        {
            TranzakciokDataGrid.ItemsSource = viewModel.Tranzakciok;
        }
    }
}
