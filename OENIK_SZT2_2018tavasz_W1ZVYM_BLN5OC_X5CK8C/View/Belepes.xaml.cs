﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Business_Logic.Interfaces;
using Business_Logic;
using Data_Layer;
using Business_Logic.Logic;

namespace OENIK_SZT2_2018tavasz_W1ZVYM_BLN5OC_X5CK8C
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class Belepes : Window
    {
        private ViewModel.BelepesViewModel viewModel;
        private ILogInLogic logInLogic;
        private AdatbazisEntities adatbazisEntities;
        /// <summary>
        /// Belepes konstruktor
        /// </summary>
        public Belepes()
        {
            InitializeComponent();
        }
        private void BejelentkezesButton_Click(object sender, RoutedEventArgs e)
        {

            try
            {
                logInLogic.LogInUser(FelhasznalonevTextBox.Text, JelszoPasswordBox.Password);
                Adminisztracio AdminisztracioTulajAblak = new Adminisztracio(FelhasznalonevTextBox.Text, this.adatbazisEntities);
                AdminisztracioTulajAblak.ShowDialog();
            }
            catch (LoginException loginException)
            {
                MessageBox.Show(loginException.Msg);
            }
            catch (InvalidOperationException invalidOperationException)
            {
                MessageBox.Show(invalidOperationException.Message);
            }
            finally
            {
                using (Task task = new Task(() => { LogLogic.WriteLogFile(); }))
                {
                    task.Start();
                    Task.WaitAll(task);
                }
            }
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            adatbazisEntities = new AdatbazisEntities();
            logInLogic = new LogInLogic(adatbazisEntities);
            viewModel = new ViewModel.BelepesViewModel();
            this.DataContext = viewModel;
        }

        private void Window_KeyDown(object sender, KeyEventArgs e)
        {
            if(e.Key == Key.Enter)
            {
                BejelentkezesButton_Click(BejelentkezesButton, null);
            }
        }
    }
}
