var indexSectionsWithContent =
{
  0: "abcdefgijklmnorstuv",
  1: "abcefiklmtu",
  2: "bdot",
  3: "acdefgijklmrsuv",
  4: "cl",
  5: "aeknv",
  6: "aclmrsu",
  7: "no"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "functions",
  4: "enums",
  5: "enumvalues",
  6: "properties",
  7: "pages"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Namespaces",
  3: "Functions",
  4: "Enumerations",
  5: "Enumerator",
  6: "Properties",
  7: "Pages"
};

