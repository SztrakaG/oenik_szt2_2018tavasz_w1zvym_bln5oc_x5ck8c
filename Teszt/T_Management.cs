﻿// <copyright file="T_Management.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Teszt
{
    using System;
    using System.Linq;
    using Business_Logic;
    using Business_Logic.Interfaces;
    using Data_Layer;
    using Data_Layer.Enums;
    using Data_Layer.Interfaces;
    using Data_Layer.Repositories;
    using NUnit.Framework;

    /// <summary>
    /// Tesztosztály(ügyfelek, autók, felhasználók)
    /// </summary>
    [TestFixture]
    public class T_Management
    {
        /// <summary>
        /// A teljes adatbázis
        /// </summary>
        private AdatbazisEntities database;

        /// <summary>
        /// Összes autó
        /// </summary>
        private ICarRepository<Auto> allCars;

        /// <summary>
        /// Az eladott autók
        /// </summary>
        private ISellCarRepository<Auto, Ugyfel, EladottAuto> soldCars;

        /// <summary>
        /// Az autókat kezelő osztály példány
        /// </summary>
        private ICarManagementLogic carManegement;

        /// <summary>
        /// Minta autó példány
        /// </summary>
        private Auto auto;

        /// <summary>
        /// Minta ügyfél példány
        /// </summary>
        private Ugyfel ugyfel;

        /// <summary>
        /// Minta kapcsolattartó példány
        /// </summary>
        private Kapcsolattarto kapcs;

        /// <summary>
        /// A felhasználókat kezelő osztály példány
        /// </summary>
        private UserManagementLogic userM;

        /// <summary>
        /// Bejelentkezéseket kezelő osztály példány
        /// </summary>
        private LogInLogic loginLogic;

        /// <summary>
        /// Tranzakciókat kezelő osztály példány
        /// </summary>
        private TransactionLogic tran;

        /// <summary>
        /// Az ügyfeleket kezelő osztály példány
        /// </summary>
        private IClientManagementLogic clientManagement;

        /// <summary>
        /// Kezdeti beállítások az összes teszthez
        /// </summary>
        [SetUp]
        public void Init()
        {
            this.database = new AdatbazisEntities();
            this.allCars = new AutoRepository(this.database);
            this.soldCars = new EladottAutoRepository(this.database);
            this.carManegement = new CarManagementLogic(this.database);
            this.clientManagement = new ClientManagementLogic(this.database);
            this.userM = new UserManagementLogic(this.database);
            this.loginLogic = new LogInLogic(this.database);
            this.tran = new TransactionLogic(this.database);
            this.auto = new Auto()
            {
                rendszam = "BVK-799",
                tipus = "Cabrio",
                alapar = 30000,
                szin = "piros",
                kolcsonzesi_ar_1napra = 3000,
                allapot = "Ujszeru",
                statusz = "Elerheto",
                felveteli_datum = DateTime.Now
            };

            this.ugyfel = new Ugyfel()
            {
                adoszam = "23587986477",
                szigszam = "459626BO",
                ugyfel_nev = "Balázs Evelin",
                torzsvasarloi_pontszam = 0
            };

            this.kapcs = new Kapcsolattarto()
            {
                kapcsolattarto_nev = "Sanyi",
                email = "magyar@test.hu",
                szigszam = "9876543210",
                telefonszam = "0123456789"

                // nincs ugyfel megadva
            };
        }

        /// <summary>
        /// Teszt: Az autókat tároló objektumok sikeressége
        /// </summary>
        [Test]
        public void T_CarManagementLogic()
        {
            Assert.That(this.allCars, Is.Not.Null);
            Assert.That(this.soldCars, Is.Not.Null);
        }

        /// <summary>
        /// Teszt: sikertelen CarAdd InvalidOperationException
        /// </summary>
        [Test]
        public void T_CarAdd()
        {
            Assert.Throws<InvalidOperationException>(() => this.allCars.Add(this.auto, "Mercedes-Bens"));
        }

        /// <summary>
        /// Teszt: sikertelen CarDelete InvalidOperationException
        ///     Van ilyen auto és függoségek miatt nem engedi törölni
        /// </summary>
        [Test]
        public void T_CarDelete()
        {
            Assert.Throws(
                typeof(InvalidOperationException),
                () => this.allCars.Delete("BVK-799"),
                "Autó nem törölheto, további függoségi viszonyok állnak fent.",
                null);
        }

        /// <summary>
        /// Teszt: sikeres Kölcsönzési ár kiszámítás
        ///     carToRentOut.kolcsonzesi_ar_1napra * days * (1 - ((renter.torzsvasarloi_pontszam / 2) / 100)
        /// </summary>
        [Test]
        public void T_CalculateCarPriceToRentOut()
        {
            var res = 3000.0 * 5.0 * (1.0 - ((0 / 2) / 100.0));
            var vart = this.carManegement.CalculateCarPriceToRentOut(this.auto, this.ugyfel, 5);
            Assert.That(vart, Is.EqualTo(res));
        }

        /// <summary>
        /// Tezst: Sikeres Eladási ár kalkulátor
        /// </summary>
        [Test]
        public void T_CalculateCarBuyingPrice()
        {
            var res = 30000.0 * (1.0 - ((0 / 2.0) / 100.0));
            var vart = this.carManegement.CalculateCarPriceToSell(this.auto, this.ugyfel);
            Assert.That(vart, Is.EqualTo(res));
        }

        /// <summary>
        /// Teszt: Sikertelen ügyfél hozzáadás
        ///     A kapcsolattartó példány hiányossága miatt
        /// </summary>
        [Test]
        public void T_ClientAdd()
        {
            Assert.Throws<InvalidOperationException>(() => this.clientManagement.Add(this.ugyfel, this.kapcs));
        }

        /// <summary>
        /// Teszt: Sikertelen törlés
        ///     Függőségek állnak fent
        /// </summary>
        [Test]
        public void T_ClientDeleteException()
        {
            Assert.Throws<InvalidOperationException>(() => this.clientManagement.Delete("46221797981"));
        }

        /// <summary>
        /// Teszt: Bejelentkezés sikeressége
        ///     felhasználó jelszava megegyezik az elvárttal
        /// </summary>
        [Test]
          public void T_Login()
        {
            this.loginLogic.LogInUser("heu.reka", "kiselefant");
            Assert.That(this.loginLogic.LoggedInUser.jelszo, Is.EqualTo(Utilities.GetHashSha256("kiselefant")));
        }

        /// <summary>
        /// Teszt: Bejelentkezés előtti alapértelmezett jogosultság kiadás
        /// </summary>
        [Test]
        public void T_LoginAccesDefault()
        {
            Assert.That(this.loginLogic.Access, Is.EqualTo(Data_Layer.Enums.LoginLevelEnum.None));
        }

        /// <summary>
        /// Teszt: Bejelentkezés után a jogosultság ellenőrzése Alkalmazott felhasználónál
        /// </summary>
        [Test]
        public void T_LoginAccesSync()
        {
            this.loginLogic.LogInUser("heu.reka", "kiselefant");
            Assert.That(this.loginLogic.Access, Is.EqualTo(LoginLevelEnum.Alkalmazott));
        }

        /// <summary>
        /// Teszt: Kijelentkezés után jogosultság visszaállítása alapértelmezettre
        /// </summary>
        [Test]
        public void T_LogoutAccesChangedToNone()
        {
            this.loginLogic.LogInUser("heu.reka", "kiselefant");
            this.loginLogic.LogOut();
            Assert.That(this.loginLogic.Access, Is.EqualTo(LoginLevelEnum.None));
        }

        /// <summary>
        /// Teszt: Rossz jelszóval való belépés
        ///     LoginException dobása
        /// </summary>
        [Test]
        public void T_LoginWrongPassford()
        {
            Assert.Throws<LoginException>(() => this.loginLogic.LogInUser("heu.reka", "1"));
        }

        /// <summary>
        /// Teszt: tranzakciók példány nem üres
        ///     mivel vannak alap adatok
        /// </summary>
        [Test]
        public void T_Transactions()
        {
            Assert.IsNotEmpty(this.tran.GetAllTransactions);
        }

        /// <summary>
        /// Teszt: felhasználók példány nem üres
        ///     mivel vannak alap felhasználók
        /// </summary>
        [Test]
        public void T_UsersNotEmpty()
        {
            Assert.IsNotEmpty(this.userM.GetAll());
        }

        /// <summary>
        /// Teszt: Az autók példány nem tartalmazza a kölcsönzésre adott autókat
        /// </summary>
        [Test]
        public void T_MegaTest1()
        {
            Assert.That(this.allCars.GetCarsByStatus(CarStatusEnum.kölcsönözve).Select(x => x).FirstOrDefault(), Is.Null);
        }

        /// <summary>
        /// Teszt: Az eladott autók megtalálhatók az allCars példányban
        ///     tesztelés egy kiválasztott próbaelemmel
        /// </summary>
        [Test]
        public void T_MegaTest2()
        {
            var res = this.soldCars.GetAll().First();
            var car = this.allCars.GetAll().Where(x => x.rendszam == res.rendszam).Select(x => x).First();

            Assert.That(res.rendszam, Is.EqualTo(car.rendszam));
        }

        /// <summary>
        /// Teszt: Az eladott autó státusza az allCars-ban "eladva"
        /// </summary>
        [Test]
        public void T_MegaTest3()
        {
            var res = this.soldCars.GetAll().First();
            var car = this.allCars.GetAll().Where(x => x.rendszam == res.rendszam).Select(x => x).First();

            Assert.That(car.statusz, Is.EqualTo(CarStatusEnum.eladva.ToString()));
        }

        /// <summary>
        /// Teszt: Adott ügyfél elemnek a pontszáma kissebb mint 5 a lekérés után is
        /// </summary>
        [Test]
        public void T_MegaTest4()
        {
            var res = this.clientManagement.GetAll().Where(x => x.torzsvasarloi_pontszam < 5).Select(x => x).FirstOrDefault();

            Assert.That(res.torzsvasarloi_pontszam, Is.LessThan(5));
        }

        /// <summary>
        /// Teszt: Jelszó kódolás ellenőrzése
        ///     Alkalmazott/Tulajdonos felhasználóra is
        /// </summary>
        /// <param name="jelsz">
        ///     Normál jelszó string
        /// </param>
        /// <param name="jelszoh">
        ///     Hashelt jelszó string
        /// </param>
        [TestCase("kiselefant", "ccc8c19cbd30ee4d0b5ffcfb41e8cd3afcae244ad0c1cd2f78ef7bb8c801fea3")]
        [TestCase("kiscica", "e4e8ac37aa9b842b55240610f7dbcfe9e52777b7d6cd8d2cd127030a9d0eac39")]
        public void T_PasswordHasher(string jelsz, string jelszoh)
        {
            string jelszo = jelsz;
            string jelszohash = jelszoh;
            Assert.That(jelszohash, Is.EqualTo(Utilities.GetHashSha256(jelszo)));
        }

        /// <summary>
        /// Teszt: Felhasználó Próbapéldán jelszava
        ///     megegyezik az adatbázisban lévő megfelelő felhasználó jelszavával
        /// </summary>
        public void T_UserGetId()
        {
            Felhasznalo felhasznalo = new Felhasznalo()
            {
                aktiv = true,
                felhasznalonev = "heu.reka",
                jelszo = Utilities.GetHashSha256("kiselefant"),
                jogosultsag = "alkalmazott"
            };
            Assert.That(felhasznalo.jelszo, Is.EqualTo(this.userM.GetById("heu.reka").jelszo));
        }
    }
}
