﻿// <copyright file="ICarManagementLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Business_Logic.Interfaces
{
    using System.Collections.Generic;
    using Data_Layer;
    using Data_Layer.Enums;
    using Data_Layer.Interfaces;

    /// <summary>
    /// Autókat kezelő interfész
    /// </summary>
    public interface ICarManagementLogic
    {
        /// <summary>
        /// Gets összes autók
        /// </summary>
        ICarRepository<Auto> AllCars { get; }

        /// <summary>
        /// Gets eladott autó
        /// </summary>
        ISellCarRepository<Auto, Ugyfel, EladottAuto> SoldCar { get; }

        /// <summary>
        /// Gets kölcsönzött autók
        /// </summary>
        IRentCarRepository<Auto, Ugyfel, KolcsonzottAuto> RentedCar { get; }

        /// <summary>
        /// Felfeszi a paraméterül kapott autót.
        /// </summary>
        /// <param name="newCar">az új autó</param>
        /// <param name="brand">az új autó márkája</param>
        void Add(Auto newCar, string brand);

        /// <summary>
        /// Törli a paraméterül kapott rendszámnak megfelelő autót.
        /// </summary>
        /// <param name="rendszam">törlendő autó rendszáma</param>
        void Delete(string rendszam);

        /// <summary>
        /// Visszadja a paraméterül kapott rendszámmal rendelkező autót.
        /// </summary>
        /// <param name="id">rendszám</param>
        /// <returns>Autó</returns>
        Auto GetById(string id);

        /// <summary>
        /// Visszadja a praméterül kapott stásszal rendelkező autókat.
        /// </summary>
        /// <param name="status">autó státusz</param>
        /// <returns>Autók</returns>
        IEnumerable<Auto> GetBySatus(CarStatusEnum status);

        /// <summary>
        /// Frissíti az autók státuszát az aktuális napnak megfelelően (kölcsönözve --> elérhető)
        /// </summary>
        void UpdateAllCarsStatus();

        /// <summary>
        /// Az adatbázis módosítását végzi, ha vesznek egy autót.
        /// </summary>
        /// <param name="carToSell">ezt az autót szeretné megvenni a vevő</param>
        /// <param name="buyer">maga a vevő</param>
        void SellCar(Auto carToSell, Ugyfel buyer);

        /// <summary>
        ///  Visszadja azt az árat amennyiért eladható az autó, továbbá figyelembe veszi a kedvezményt is.
        /// </summary>
        /// <param name="carToSell">eladó autó</param>
        /// <param name="buyer">vevő</param>
        /// <returns>eladási ár</returns>
        decimal CalculateCarPriceToSell(Auto carToSell, Ugyfel buyer);

        /// <summary>
        /// Az adatbázis módosítását végzi, ha kölcsönöznek egy autót.
        /// </summary>
        /// <param name="carToRentOut">kölcsönzendő autó</param>
        /// <param name="renter">bérlő</param>
        /// <param name="days">bérleti napok száma</param>
        void RentOutCar(Auto carToRentOut, Ugyfel renter, int days);

        /// <summary>
        /// Visszaadja a bérlendő autó árát figyelembevéve a kedvezményt is.
        /// </summary>
        /// <param name="carToRentOut">bérlendő autó</param>
        /// <param name="renter">bérlő</param>
        /// <param name="days">bérlendő napok száma</param>
        /// <returns>kölcsönzési ár</returns>
        decimal CalculateCarPriceToRentOut(Auto carToRentOut, Ugyfel renter, int days);
    }
}
