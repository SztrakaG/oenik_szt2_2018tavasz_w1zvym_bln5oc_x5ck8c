﻿// <copyright file="ITransactionLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Business_Logic.Interfaces
{
    using System.Collections.Generic;
    using Data_Layer;

    /// <summary>
    /// Tranzakció logika interfésze
    /// </summary>
    public interface ITransactionLogic
    {
        /// <summary>
        /// Gets összes tranzakció
        /// </summary>
        List<Tranzakcio> GetAllTransactions { get; }
    }
}
