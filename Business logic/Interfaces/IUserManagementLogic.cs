﻿// <copyright file="IUserManagementLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Business_Logic.Interfaces
{
    using System.Collections.Generic;
    using Data_Layer;

    /// <summary>
    /// Felhasználókat kezelő innterfész
    /// </summary>
    public interface IUserManagementLogic
    {
        /// <summary>
        /// Az összes felhasználót adja vissza
        /// </summary>
        /// <returns>Felhasználók</returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate", Justification = "A method is better here to emphasize complexity / prepare users for longer return time.")]
        IEnumerable<Felhasznalo> GetAll();

        /// <summary>
        /// Új felhasználó felvétele
        /// </summary>
        /// <param name="newUser">új felhasználó</param>
        void Add(Felhasznalo newUser);

        /// <summary>
        /// Paraméterül kapott felhasználónévvel rendelkező felhasználó törlése
        /// </summary>
        /// <param name="felhasznalonev">felhasználónév</param>
        void Delete(string felhasznalonev);

        /// <summary>
        /// Paraméterül kapott felhasználónévvel rendelkező felhasználót adja vissza
        /// </summary>
        /// <param name="id">azonosító</param>
        /// <returns>Felhasználó</returns>
        Felhasznalo GetById(string id);
    }
}
