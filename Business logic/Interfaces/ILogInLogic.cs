﻿// <copyright file="ILogInLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Business_Logic.Interfaces
{
    using Data_Layer;
    using Data_Layer.Enums;

    /// <summary>
    /// Bejelentkezést kezelő interfész
    /// </summary>
    public interface ILogInLogic
    {
        /// <summary>
        /// Gets bejelentkezett felhasználó hozzáférési szintje
        /// </summary>
        LoginLevelEnum Access { get; }

        /// <summary>
        /// Gets bejlentkezett felhasználó
        /// </summary>
        Felhasznalo LoggedInUser { get; }

        /// <summary>
        /// Aktuálisan belépett felhasználó kijelentkeztetése.
        /// </summary>
        void LogOut();

        /// <summary>
        /// Felhasználó bejelentkeztetése
        /// </summary>
        /// <param name="username">felhasználó felhasználóneve</param>
        /// <param name="pass">felhasználó jelszava</param>
        void LogInUser(string username, string pass);
    }
}
