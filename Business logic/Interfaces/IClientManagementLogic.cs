﻿// <copyright file="IClientManagementLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Business_Logic.Interfaces
{
    using System.Collections.Generic;
    using Data_Layer;
    using Data_Layer.Interfaces;

    /// <summary>
    /// Ügyfeleket kezelő interfész
    /// </summary>
    public interface IClientManagementLogic
    {
        /// <summary>
        /// Gets összes ügyfelek
        /// </summary>
        IClientRepository<Ugyfel, Kapcsolattarto> Clients { get; }

        /// <summary>
        /// Új ügyfél felvétele a rendszerbe
        /// </summary>
        /// <param name="newClient">új ügyfél</param>
        /// <param name="contect">új ügyfél kapcsolattartója</param>
        void Add(Ugyfel newClient, Kapcsolattarto contect);

        /// <summary>
        /// Paraméterül kapott adószámmal rendelkező ügyfél törlése
        /// </summary>
        /// <param name="adoszam">törlendő ügyfél adószáma</param>
        void Delete(string adoszam);

        /// <summary>
        /// Paraméterül kapott adószámmal rendelkező ügyfelet adja vissza
        /// </summary>
        /// <param name="id">adószám</param>
        /// <returns>Ügyfél</returns>
        Ugyfel GetById(string id);

        /// <summary>
        /// Az összes ügyfelet adja vissza
        /// </summary>
        /// <returns>Ügyfelek</returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate", Justification = "A method is better here to emphasize complexity / prepare users for longer return time.")]
        IEnumerable<Ugyfel> GetAll();
    }
}
