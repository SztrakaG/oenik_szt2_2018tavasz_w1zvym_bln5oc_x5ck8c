﻿// <copyright file="IStatementsLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Business_Logic.Interfaces
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Kimutatás logika interfésze
    /// </summary>
    public interface IStatementsLogic
    {
        /// <summary>
        /// Kölcsönzött autók száma adott intervallumban
        /// </summary>
        /// <param name="from">mettől</param>
        /// <param name="till">meddig</param>
        /// <returns>kölcsönöztt autók száma</returns>
        int RentedCarsCount(DateTime from, DateTime till);

        /// <summary>
        /// Eladott autók száma adott intervallumban
        /// </summary>
        /// <param name="from">mettől</param>
        /// <param name="till">meddig</param>
        /// <returns>eladott autók száma</returns>
        int SoldCarsCount(DateTime from, DateTime till);

        /// <summary>
        /// Eladott autókból származó bevétel adott intervallumban
        /// </summary>
        /// <param name="from">mettől</param>
        /// <param name="till">meddig</param>
        /// <returns>eladott autók összértéke</returns>
        int SoldCarsIncome(DateTime from, DateTime till);

        /// <summary>
        /// Kölcsönzött autókból származó bevétel adott intervallumban
        /// </summary>
        /// <param name="from">mettől</param>
        /// <param name="till">meddig</param>
        /// <returns>kölcsönzött autókból származó jövedelem</returns>
        int RentedCarsIncome(DateTime from, DateTime till);
    }
}
