﻿// <copyright file="LogInLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Business_Logic
{
    using Business_Logic.Interfaces;
    using Business_Logic.Logic;
    using Data_Layer;
    using Data_Layer.Enums;
    using Data_Layer.Interfaces;
    using Data_Layer.Repositories;

    /// <summary>
    /// Bejelentkezésért felelős osztály
    /// </summary>
    public class LogInLogic : ILogInLogic
    {
        /// <summary>
        /// Engedélyezett bejelentkezési próbák száma
        /// </summary>
        private const int MAXTRIES = 3;

        /// <summary>
        /// hibajelzések üzenetei
        /// </summary>
        private string msg;

        /// <summary>
        /// Elhasznált bejelentkezési próbák száma
        /// </summary>
        private int loginTries = 0;

        /// <summary>
        /// FElhasználók
        /// </summary>
        private IUserRepository<Felhasznalo> users;

        /// <summary>
        /// Bejelentkezett felhasználó hozzáférési szintje
        /// </summary>
        private LoginLevelEnum access;

        /// <summary>
        /// Bejlentkezett felhasználó
        /// </summary>
        private Felhasznalo loggedInUser;

        /// <summary>
        /// Initializes a new instance of the <see cref="LogInLogic"/> class.
        /// Konstruktor
        /// </summary>
        /// <param name="entities">adatbázis</param>
        public LogInLogic(AdatbazisEntities entities)
        {
            this.users = new FelhasznaloRepository(entities);
            this.access = LoginLevelEnum.None;
        }

        /// <summary>
        /// Gets aktuálisan bejelentkezett felhasználó hozzáférési szintje
        /// </summary>
        public LoginLevelEnum Access
        {
            get
            {
                return this.access;
            }
        }

        /// <summary>
        /// Gets aktuálisan bejelentkezett felhasználó.
        /// </summary>
        public Felhasznalo LoggedInUser
        {
            get
            {
                return this.loggedInUser;
            }
        }

        /// <summary>
        /// Aktuálisan belépett felhasználó kijelentkeztetése.
        /// </summary>
        public void LogOut()
        {
            this.loggedInUser = null;
            this.access = LoginLevelEnum.None;
        }

        /// <summary>
        /// Felhasználó bejelentkeztetése
        /// </summary>
        /// <param name="username">felhasználó felhasználóneve</param>
        /// <param name="pass">felhasználó jelszava</param>
        public void LogInUser(string username, string pass)
        {
            this.loginTries++;
            try
            {
                // ha a bejelentkezési próbák száma < 4 && jók az adatok && aktív a profil || (jók az adatok és vezető próbál belépni)
                if ((this.loginTries < MAXTRIES + 1 && this.users.VerifyUser(username, pass) && this.users.GetById(username).aktiv) || (this.users.VerifyUser(username, pass) && Utilities.JogosultsagToEnum(this.users.GetById(username).jogosultsag) == LoginLevelEnum.Vezeto))
                {
                    this.access = Utilities.JogosultsagToEnum(this.users.GetById(username).jogosultsag);
                    this.loggedInUser = this.users.GetById(username);
                    this.loginTries = 0; // ki kell nullázni a számlálót
                    LogLogic.Log = "Sikeres belépés " + username;
                }
                else
                {
                    this.access = LoginLevelEnum.AccesDenied;
                    if (this.loginTries >= MAXTRIES)
                    {
                        this.msg = "Hibás próbálkozások száma elérte a maximumot! Forduljon a munkáltatójához!";
                        try
                        {
                            if (this.users.GetById(username) != null)
                            {
                                this.users.GetById(username).aktiv = false;
                            }
                        }
                        catch (LoginException)
                        {
                            throw new LoginException(username, this.msg);
                        }
                        finally
                        {
                            LogLogic.Log = this.msg;
                            throw new LoginException(username, this.msg);
                        }
                    }
                    else if (!this.users.VerifyUser(username, pass))
                    {
                        this.msg = "Nincs ilyen felhasználónév és jelszó páros. Forduljon a munkáltatójához!";
                        LogLogic.Log = this.msg;
                        throw new LoginException(username, this.msg);
                    }
                    else if (!this.users.GetById(username).aktiv)
                    {
                        this.msg = "A felhasználói fiókja jelenleg nem aktív. Forduljon a munkáltatójához!";
                        LogLogic.Log = this.msg;
                        throw new LoginException(username, this.msg);
                    }
                }
            }
            catch (LoginException)
            {
                throw;
            }
        }
    }
}
