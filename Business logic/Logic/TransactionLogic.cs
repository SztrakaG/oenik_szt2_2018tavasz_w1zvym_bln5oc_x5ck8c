﻿// <copyright file="TransactionLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Business_Logic
{
    using System.Collections.Generic;
    using Data_Layer;
    using Data_Layer.Interfaces;
    using Data_Layer.Repositories;
    using Interfaces;

    /// <summary>
    /// Tranzakciók logikája
    /// </summary>
    public class TransactionLogic : ITransactionLogic
    {
        private ITransactionRepository tranzakciok;

        /// <summary>
        /// Initializes a new instance of the <see cref="TransactionLogic"/> class.
        /// Tranzakciókat létrehozó konstruktor
        /// </summary>
        /// <param name="entities">adatbázis</param>
        public TransactionLogic(AdatbazisEntities entities)
        {
            this.tranzakciok = new TransactionRepository(entities);
        }

        /// <summary>
        /// Gets összes tranzakció
        /// </summary>
        public List<Tranzakcio> GetAllTransactions
        {
            get { return this.tranzakciok.Tranzakciok; }
        }
    }
}
