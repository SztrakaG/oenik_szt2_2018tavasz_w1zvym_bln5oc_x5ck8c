﻿// <copyright file="UserManagementLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Business_Logic
{
    using System;
    using System.Collections.Generic;
    using Data_Layer;
    using Data_Layer.Repositories;
    using Interfaces;

    /// <summary>
    /// FElhasznélókat kezelő osztály
    /// </summary>
    public class UserManagementLogic : IUserManagementLogic
    {
        /// <summary>
        /// Felhasználók
        /// </summary>
        private FelhasznaloRepository users;

        /// <summary>
        /// Initializes a new instance of the <see cref="UserManagementLogic"/> class.
        /// Konstruktor
        /// </summary>
        /// <param name="entities">adatbázis</param>
        public UserManagementLogic(AdatbazisEntities entities)
        {
            this.users = new FelhasznaloRepository(entities);
        }

        /// <summary>
        /// Új felhasználó felvétele
        /// </summary>
        /// <param name="newUser">új felhasználó</param>
        public void Add(Felhasznalo newUser)
        {
            try
            {
                this.users.Add(newUser);
            }
            catch (InvalidOperationException)
            {
                throw;
            }
        }

        /// <summary>
        /// Paraméterül kapott felhasználónévvel rendelkező felhasználó törlése
        /// </summary>
        /// <param name="felhasznalonev">felhasználónév</param>
        public void Delete(string felhasznalonev)
        {
            this.users.Delete(felhasznalonev);
        }

        /// <summary>
        /// Az összes felhasználót adja vissza
        /// </summary>
        /// <returns>Felhasználók</returns>
        public IEnumerable<Felhasznalo> GetAll()
        {
            return this.users.GetAll();
        }

        /// <summary>
        /// Paraméterül kapott felhasználónévvel rendelkező felhasználót adja vissza
        /// </summary>
        /// <param name="id">azonosító</param>
        /// <returns>Felhasználó</returns>
        public Felhasznalo GetById(string id)
        {
            return this.users.GetById(id);
        }
    }
}
