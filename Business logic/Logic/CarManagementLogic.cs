﻿// <copyright file="CarManagementLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Business_Logic
{
    using System;
    using System.Collections.Generic;
    using Data_Layer;
    using Data_Layer.Enums;
    using Data_Layer.Interfaces;
    using Data_Layer.Repositories;
    using Interfaces;

    /// <summary>
    /// Autókat kezelő osztály (eladás, kölcsönzés, összes autó)
    /// </summary>
    public class CarManagementLogic : ICarManagementLogic
    {
        /// <summary>
        /// Összes autó
        /// </summary>
        private ICarRepository<Auto> allCars;

        /// <summary>
        /// Eladott autók
        /// </summary>
        private ISellCarRepository<Auto, Ugyfel, EladottAuto> soldCars;

        /// <summary>
        /// Kölcsönzött autók
        /// </summary>
        private IRentCarRepository<Auto, Ugyfel, KolcsonzottAuto> rentedCars;

        /// <summary>
        /// Initializes a new instance of the <see cref="CarManagementLogic"/> class.
        /// Konstruktor
        /// </summary>
        /// <param name="entities">adaatbázis</param>
        public CarManagementLogic(AdatbazisEntities entities)
        {
            this.allCars = new AutoRepository(entities);
            this.soldCars = new EladottAutoRepository(entities);
            this.rentedCars = new KolcsonzottAutoRepository(entities);
        }

        /// <summary>
        /// Gets az összes autó
        /// </summary>
        public ICarRepository<Auto> AllCars
        {
            get
            {
                return this.allCars;
            }
        }

        /// <summary>
        /// Gets eladott autók
        /// </summary>
        public ISellCarRepository<Auto, Ugyfel, EladottAuto> SoldCar
        {
            get
            {
                return this.soldCars;
            }
        }

        /// <summary>
        /// Gets kölcsönzött autók
        /// </summary>
        public IRentCarRepository<Auto, Ugyfel, KolcsonzottAuto> RentedCar
        {
            get
            {
                return this.rentedCars;
            }
        }

        /// <summary>
        /// Felveszi a paraméterül kapott autót.
        /// </summary>
        /// <param name="newCar">az új autó</param>
        /// <param name="brand">az új autó márkája</param>
        public void Add(Auto newCar, string brand)
        {
            try
            {
                this.allCars.Add(newCar, brand);
            }
            catch (InvalidOperationException)
            {
                throw;
            }
        }

        /// <summary>
        /// Törli a paraméterül kapott rendszámnak megfelelő autót.
        /// </summary>
        /// <param name="rendszam">törlendő autó rendszáma</param>
        public void Delete(string rendszam)
        {
            try
            {
                this.allCars.Delete(rendszam);
            }
            catch (InvalidCastException)
            {
                throw;
            }
        }

        /// <summary>
        /// Az adatbázis módosítását végzi, ha vesznek egy autót.
        /// </summary>
        /// <param name="carToSell">ezt az autót szeretné megvenni a vevő</param>
        /// <param name="buyer">maga a vevő</param>
        public void SellCar(Auto carToSell, Ugyfel buyer)
        {
            this.soldCars.Add(carToSell, buyer);
        }

        /// <summary>
        ///  Visszadja azt az árat amennyiért eladható az autó, továbbá figyelembe veszi a kedvezményt is.
        /// </summary>
        /// <param name="carToSell">eladó autó</param>
        /// <param name="buyer">vevő</param>
        /// <returns>ekadási ár</returns>
        public decimal CalculateCarPriceToSell(Auto carToSell, Ugyfel buyer)
        {
            return carToSell.alapar * (1 - ((buyer.torzsvasarloi_pontszam / 2) / 100));
        }

        /// <summary>
        /// Az adatbázis módosítását végzi, ha kölcsönöznek egy autót.
        /// </summary>
        /// <param name="carToRentOut">kölcsönzendő autó</param>
        /// <param name="renter">bérlő</param>
        /// <param name="days">bérleti napok száma</param>
        public void RentOutCar(Auto carToRentOut, Ugyfel renter, int days)
        {
            this.rentedCars.Add(carToRentOut, renter, days);
        }

        /// <summary>
        /// Visszaadja a bérlendő autó árát figyelembevéve a kedvezményt is.
        /// </summary>
        /// <param name="carToRentOut">bérlendő autó</param>
        /// <param name="renter">bérlő</param>
        /// <param name="days">bérlendő napok száma</param>
        /// <returns>kölcsönzési ár</returns>
        public decimal CalculateCarPriceToRentOut(Auto carToRentOut, Ugyfel renter, int days)
        {
            return carToRentOut.kolcsonzesi_ar_1napra * days * (1 - ((renter.torzsvasarloi_pontszam / 2) / 100));
        }

        /// <summary>
        /// Visszadja a paraméterül kapott rendszámmal rendelkező autót.
        /// </summary>
        /// <param name="id">rendszám</param>
        /// <returns>Autó</returns>
        public Auto GetById(string id)
        {
            return this.allCars.GetById(id);
        }

        /// <summary>
        /// Visszadja a praméterül kapott stásszal rendelkező autókat.
        /// </summary>
        /// <param name="status">autó státusz</param>
        /// <returns>Autók</returns>
        public IEnumerable<Auto> GetBySatus(CarStatusEnum status)
        {
            return this.allCars.GetCarsByStatus(status);
        }

        /// <summary>
        /// Frissíti az autók státuszát az aktuális napnak megfelelően (kölcsönözve --> elérhető)
        /// </summary>
        public void UpdateAllCarsStatus()
        {
            this.allCars.UpdateSatus();
        }
    }
}
