﻿// <copyright file="StatementsLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Business_Logic.Logic
{
    using System;
    using Data_Layer;
    using Data_Layer.Interfaces;
    using Interfaces;

    /// <summary>
    /// Kimutatások logika
    /// </summary>
    public class StatementsLogic : IStatementsLogic
    {
        private IStatements kimutatasok;

        /// <summary>
        /// Initializes a new instance of the <see cref="StatementsLogic"/> class.
        /// Konstruktor
        /// </summary>
        /// <param name="entities">adatbázis</param>
        public StatementsLogic(AdatbazisEntities entities)
        {
            this.kimutatasok = new StatementsRepository(entities);
        }

        /// <summary>
        /// Kölcsönzött autók száma adott intervallumba
        /// </summary>
        /// <param name="from">intervallum eleje</param>
        /// <param name="till">intervallum vége</param>
        /// <returns>darab</returns>
        public int RentedCarsCount(DateTime from, DateTime till)
        {
            return this.kimutatasok.RentedCarsCount(from, till);
        }

        /// <summary>
        /// Eladott autók száma, adott intervallumba
        /// </summary>
        /// <param name="from">intervallum eleje</param>
        /// <param name="till">intervallum vége</param>
        /// <returns>darab</returns>
        public int SoldCarsCount(DateTime from, DateTime till)
        {
            return this.kimutatasok.SoldCarsCount(from, till);
        }

        /// <summary>
        /// Eladott autókból  származó bevétel adott intervallumba
        /// </summary>
        /// <param name="from">intervallum eleje</param>
        /// <param name="till">intervallum vége</param>
        /// <returns>bevétel</returns>
        public int SoldCarsIncome(DateTime from, DateTime till)
        {
            return this.kimutatasok.SoldCarsIncome(from, till);
        }

        /// <summary>
        /// Kölcsönzött auatókból származó bevétel adott intervallumba
        /// </summary>
        /// <param name="from">intervallum eleje</param>
        /// <param name="till">intervallum vége</param>
        /// <returns>bevétel</returns>
        public int RentedCarsIncome(DateTime from, DateTime till)
        {
            return this.kimutatasok.RentedCarsIncome(from, till);
        }
    }
}
