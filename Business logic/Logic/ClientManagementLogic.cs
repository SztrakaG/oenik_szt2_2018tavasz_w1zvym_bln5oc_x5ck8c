﻿// <copyright file="ClientManagementLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Business_Logic
{
    using System;
    using System.Collections.Generic;
    using Data_Layer;
    using Data_Layer.Interfaces;
    using Data_Layer.Repositories;
    using Interfaces;

    /// <summary>
    /// Ügyfeleket kezelő osztály
    /// </summary>
    public class ClientManagementLogic : IClientManagementLogic
    {
        /// <summary>
        /// Ügyfelek
        /// </summary>
        private IClientRepository<Ugyfel, Kapcsolattarto> clients;

        /// <summary>
        /// Initializes a new instance of the <see cref="ClientManagementLogic"/> class.
        /// Konstruktor.
        /// </summary>
        /// <param name="entities">adatbázis</param>
        public ClientManagementLogic(AdatbazisEntities entities)
        {
            this.clients = new UgyfelRepository(entities);
        }

        /// <summary>
        /// Gets kliensek
        /// </summary>
        public IClientRepository<Ugyfel, Kapcsolattarto> Clients
        {
            get
            {
                return this.clients;
            }
        }

        /// <summary>
        /// Új ügyfél felvétele a rendszerbe
        /// </summary>
        /// <param name="newClient">új ügyfél</param>
        /// <param name="contect">új ügyfél kapcsolattartója</param>
        public void Add(Ugyfel newClient, Kapcsolattarto contect)
        {
            try
            {
                this.clients.Add(newClient, contect);
            }
            catch (InvalidOperationException)
            {
                throw;
            }
        }

        /// <summary>
        /// Paraméterül kapott adószámmal rendelkező ügyfél törlése
        /// </summary>
        /// <param name="adoszam">törlendő ügyfél adószáma</param>
        public void Delete(string adoszam)
        {
            try
            {
                this.clients.Delete(adoszam);
            }
            catch (InvalidOperationException)
            {
                throw;
            }
        }

        /// <summary>
        /// Paraméterül kapott adószámmal rendelkező ügyfelet adja vissza
        /// </summary>
        /// <param name="id">adószám</param>
        /// <returns>Ügyfél</returns>
        public Ugyfel GetById(string id)
        {
            return this.clients.GetById(id);
        }

        /// <summary>
        /// Az összes ügyfelet adja vissza
        /// </summary>
        /// <returns>Ügyfelek</returns>
        public IEnumerable<Ugyfel> GetAll()
        {
            return this.clients.GetAll();
        }
    }
}
