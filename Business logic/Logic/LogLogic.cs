﻿// <copyright file="LogLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Business_Logic.Logic
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using Business_Logic.Interfaces;

    /// <summary>
    /// Logolást támogató osztály
    /// </summary>
    public static class LogLogic
    {
        private static string log;

        /// <summary>
        /// Gets or sets log bejegzés
        /// </summary>
        public static string Log
        {
            get
            {
                return log;
            }

            set
            {
                log = DateTime.Now + " | " + value;
            }
        }

        /// <summary>
        /// Log bejegyzések fájlba írása
        /// </summary>
        public static void WriteLogFile()
        {
            string[] s = new string[1];
            s[0] = log;
            File.AppendAllLines("log.txt", s);
        }
    }
}