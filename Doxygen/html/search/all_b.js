var searchData=
[
  ['loggedinuser',['LoggedInUser',['../interface_business___logic_1_1_interfaces_1_1_i_log_in_logic.html#a481e4f6d13a161d9648e470d9594ae36',1,'Business_Logic.Interfaces.ILogInLogic.LoggedInUser()'],['../class_business___logic_1_1_log_in_logic.html#a9275cb5eb7e3efa083c456b9ddb8d949',1,'Business_Logic.LogInLogic.LoggedInUser()']]],
  ['loginexception',['LoginException',['../class_data___layer_1_1_login_exception.html',1,'Data_Layer.LoginException'],['../class_data___layer_1_1_login_exception.html#ae0b9aadd426204668d4d0e6a01be11d6',1,'Data_Layer.LoginException.LoginException()']]],
  ['loginlevelenum',['LoginLevelEnum',['../namespace_data___layer_1_1_enums.html#afd955b182b99446886ffb997777595fb',1,'Data_Layer::Enums']]],
  ['loginlogic',['LogInLogic',['../class_business___logic_1_1_log_in_logic.html',1,'Business_Logic.LogInLogic'],['../class_business___logic_1_1_log_in_logic.html#aaf11738506ab9206812872183d4dc07b',1,'Business_Logic.LogInLogic.LogInLogic()']]],
  ['loginuser',['LogInUser',['../interface_business___logic_1_1_interfaces_1_1_i_log_in_logic.html#ac09d9e734a393303ac9aa29d3d0e9776',1,'Business_Logic.Interfaces.ILogInLogic.LogInUser()'],['../class_business___logic_1_1_log_in_logic.html#a6c6099f0e13f3737105dd93c152f0c6f',1,'Business_Logic.LogInLogic.LogInUser()']]],
  ['logout',['LogOut',['../interface_business___logic_1_1_interfaces_1_1_i_log_in_logic.html#a4d28a362a849fd9b2191e835b8cf50fa',1,'Business_Logic.Interfaces.ILogInLogic.LogOut()'],['../class_business___logic_1_1_log_in_logic.html#aeb37e94c72a8db9af05e52727ce7ebc8',1,'Business_Logic.LogInLogic.LogOut()']]]
];
